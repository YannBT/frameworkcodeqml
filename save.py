import pandas as pd
from class_model import Model # to check instance type
from class_mlp import Mlp # to check instance type
from class_hyper import Hyper
from class_data import Data
from datetime import datetime, timedelta
import version
import os
from Global import PATH_OUTPUTS, EARLY_STOPPING_THR
import torch

# INPUT: instances of Model, Hyper and Data + list of exec times
# OUTPUT: none
# ACTION: saves the parameters of Model

# TODO: uniformise wrt classical/quantum
def save(dic, model, hyper, data, times):
    assert(isinstance(model, Model) or isinstance(model, Mlp))
    assert(isinstance(hyper, Hyper))
    assert(isinstance(data,  Data))
    
    root = PATH_OUTPUTS
    timestamps = [datetime.fromtimestamp(t).strftime('%d-%m-%Y-%H-%M-%S') for t in times]

    # Create tag and output dir
    tag  = create_tag(dic)
    path = create_tree(root, dic, model.nb_params)
    model.set_path(path)

    # Save path to a file in /outputs-DATE/ to avoid crawling
    all_paths = open(root + '/all_paths.txt', 'a')
    all_paths.write(path + '\n')
    all_paths.flush()
    all_paths.close()

    ### Create data dictionnaries ###
    P = [p.detach() for p in model.Params] if dic['type'] == 'Quantum' else model.Params
    # Create relevant dataframes
    dfParams = pd.DataFrame({'Params': P})
    dfLosses = pd.DataFrame({'Train loss' : model.TrainLosses, 'Test loss': model.TestLosses})
    dfTrain = pd.DataFrame({'Train inputs': data.train_inputs, 'Train outputs': data.train_outputs})
    dfTest  = pd.DataFrame({'Test inputs': data.test_inputs, 'Test outputs': data.test_outputs})
    # Save
    dfParams.to_csv(path + 'params_df.csv')
    dfLosses.to_csv(path + 'losses_df.csv')
    dfTrain.to_csv(path + 'train_data_df.csv')
    dfTest.to_csv(path + 'test_data_df.csv')

    # same but without torch tensors
    # FIXME: useless
    #dfDataNumpy = pd.DataFrame({'Train inputs': [d.numpy() for d in data.train_inputs],
    #    'Train outputs': [d.numpy() for d in data.train_outputs],
    #    'Test inputs': [d.numpy() for d in data.test_inputs],
    #    'Test outputs': [d.numpy() for d in data.test_outputs],
    #    })
    #dfDataNumpy.to_csv(path + 'data_df_np.csv')

    ### Human-readable summaries ###
    out_dic =  {
        'Version of the code': version.get_version(),
        'Tag': tag,
        #'Timestamp run session': 0000,
        'Start date': timestamps[0],
        'End date': timestamps[1],
        'Runtime (h:m:s)': timedelta(seconds=times[1]-times[0]),
        'Number of epochs': hyper.epochs,
        'Number of epochs consumed': len(model.Params),
        'Early stopping threshold': EARLY_STOPPING_THR}
    
    if dic['type'] == 'Classical':
        cdic = {'Shape': dic['shape'],
                'Activation': dic['activation'],
                'MaxNeuron' : dic['maxNeuron'],
                'Number of neurons (total)': model.nb_neurons_list,
                'Number of params (total)': model.nb_params,
                'Number of neurons (list)': model.nb_neurons_list,
                'Number of params (list)': model.nb_params_list
                }
        out_dic = {**out_dic, **cdic}
    elif dic['type'] == 'Quantum':
        qdic = {'Circuit ID': model.circuit,
                'Circuit name': model.name,
                'Circuit seq': model.seq,
                'Number of gates': model.nb_gates,
                'Number of angles (total)': model.nb_angles,
                'Number of params (total)': model.nb_params,
                'Number of angles (list)': model.list_nb_angles,
                'Number of params (list)': model.list_nb_params}
        out_dic = {**out_dic, **qdic}
    
    out_dic = {**out_dic, **{
        'Optimiser': hyper.Opt,
        'Loss function': hyper.Loss,
        'Final loss (train)': model.TrainLosses[-1],
        'Final loss (test)': model.TestLosses[-1],
        'Data name': data.name,
        'Data number (train)': data.nb_traindata,
        'Data number (test)': data.nb_testdata,
        'Data number (tot)': data.nb_data,
        'Batch size': data.batch_size,
        'Data dimension (input)': data.dim_input,
        'Data dimension (output)': data.dim_output
        }}


    # Write out the human-readable description of the model's dict
    dicFile = open(path + 'summary.meta', 'w')
    for key, val in out_dic.items():
        dicFile.write(key + ':\t' + str(val) + '\n')
    dicFile.close()

    ## Write out the model and data dicts for easy regeneration of the model
    ##import json
    #modelDic = open(path + 'model.dic', 'w')
    ##modelDic.write(json.dumps(dic))
    #for key, val in dic.items():
    #    modelDic.write(key + ':\t' + str(val) + '\n')
    #modelDic.close()
    #dataDic = open(path + 'data.dic', 'w')
    ##dataDic.write(json.dumps(data.dict))
    #for key, val in data.dic.items():
    #    dataDic.write(key + ':\t' + str(val) + '\n')
    #dataDic.close()

    # Write out MLP
    if dic['type'] == 'Classical':
        torch.save(model.state_dict(), path + 'torch_save.pth')

def create_tag(dic):
    tag = []
    for key, val in dic.items():
        if type(val) == list:
            val = '-'.join(val)
        tag.append(str(val))
    tag = '_'.join(tag)
    return tag

# Create the folder tree for this model. Returns the path to the created folder. Basically same as create_dir() but with a tree structure.
def create_tree(root, dic, nb_params): 
    path = root
    i = 1
    # Create the tree corresponding the dic, do nothing if already existent
    for key, val in dic.items():
        if type(val) == list:
            val = '-'.join(val)
        path += key + '_' + str(val) + '/'
        try:
            os.mkdir(path)
        except: #dir already exists
            continue

    # Add the number of parameters of the model, convenient for meta-analysis
    path += 'nbParams_' + str(nb_params) + '/'
    try:
        os.mkdir(path)
    except: #dir already exists
        pass

    # Create a leaf for the current run of dic
    while os.path.exists(path + 'run_' + str(i).zfill(6)):
        i += 1
    path += 'run_' + str(i).zfill(6)
    os.mkdir(path)
    return path + '/'

# Create the folder for this model. If already existent create a new folder. Returns the path to the created folder
# NOTE: this is deprecated because it makes disgustingly lengthy path names
'''
def create_dir(root, tag):
    try: 
        os.mkdir(root + tag)
        path = root + tag
    except:
        i = 1
        while os.path.exists(root + tag + str(i)):
            i += 1
        os.mkdir(root + tag + str(i))
        path = root + tag + str(i)
    return path + '/'
'''
