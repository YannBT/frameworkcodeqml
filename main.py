import torch
import pennylane as qml
from tqdm import tqdm
import matplotlib.pyplot as plt
import numpy as np
#import challenges_1d as ch1
import plt_params
from datetime import datetime
from time import time as tm
import os
from analytical import calc_expr, calc_angles
from itertools import product
from class_model import Model
from class_mlp import Mlp
from class_data import Data
from class_hyper import Hyper
import convenience_mlp as convMLP #FIXME: this looks too intricate
#import convenience_qnn as convQNN #FIXME: this looks too intricate
import convenience_functions as conv #FIXME: this looks too intricate
from save import save
from plots import plot
from copy import deepcopy #only to append to Params
from torchsummary import summary
import Global
from joblib import Parallel, delayed
import circuits 

# Import the dictionnaries of parameters sets + ``global'' values
from inputs import SharedDic, ClassicalDic, QuantumDic
from inputs import Types, Passes, epochs

# Conveniences
today = Global.TODAY
nprocs = Global.NPROCS
early_stopping_thr = Global.EARLY_STOPPING_THR # stopping criterion


#################

# Fetch/calculate the analytical expressions corresponding to the circuits
#Exprs = {}
#for c in Circuits:
#    # load the result if it exists
#    # TODO: try fetching the .tex
#    # if success, lambdify
#
#    # If not, calc and store it
#    name, seq = circuits.name(c)
#    # generate the full sequence of rotations
#    s = []
#    for i in range(Qubits[0]):
#        s += seq
#    seq = s[:Qubits[0]]
#    Exprs[c] = calc_expr(seq)
#
#    # test
#    x = np.linspace(-5,5,3)
#    a = calc_angles([-1, np.pi/2], x)
#    print('seq ', seq)
#    print('angles ', a)
#    print('expr ', Exprs[c](*a))
#    #plt.plot(x, Exprs[c](*a))
#    #plt.show()


path = os.getcwd() + '/outputs-' + today + '/' #TODO: proper organisation for storing the outputs
if not os.path.isdir(path):
    os.mkdir(path)

def loopModel(paramSet, Dic, idx):
    '''
    paramSet: tuple of parameters defining the model
    Dic: keys are in the same order as the values of paramSet
    idx: index of the batch
    '''
    keys = Dic.keys() #workaround bc pickle cannot handle Dic.keys()
    
    # Generate the dictionnary for this parameter set
    dic = {}
    for k, v in zip(keys, paramSet):
        newKey = k[0].lower() + k[1:-1] # emphasise the new key refers to a single value and not a list
        dic[newKey] = v             

    # Consistency check
    if not(conv.isConsistent(dic)): return 0

    # Init classes
    data = Data(dic['dataName'], train_prop=dic['trainProp'], batch_size=dic['batchSize']) 
    
    if dic['type'] == 'Classical':
        layer_params = convMLP.gen_archi(dic['depth'], dic['activation'], dic['maxNeuron'], data.dim_output, dic['shape']) 
        model = Mlp(data.dim_input, layer_params)
        #print(summary(model, (1,1))) #can be usefull but needs to work for qnn too
        hyper = Hyper(epochs, dic['opt'], dic['cost'], model.parameters())
    elif dic['type'] == 'Quantum':
        #TODO: write an equivalent to gen_archi
        model = Model(dic['circuit'], dic['seq'], dic['qubit'], dic['depth'], dic['cost'], data)
        hyper = Hyper(epochs, dic['opt'], dic['cost'], [model.params]) #FIXME: needing to pass a list is wrong, but currently constrained by the mlp. TODO: fix this for Model
   

    start = tm()
    for epoch in tqdm(range(epochs), desc='Epochs', position=1, leave=False):
        if dic['type'] == 'Classical': model.train()

        # Training
        model.loss = 0
        for in_train, out_train in data.train_loader:
            hyper.Opt.zero_grad()
            pred = model.forward(in_train)
            batch_loss = hyper.Loss(pred.squeeze(), out_train.squeeze()) #NOTE: is squeezing really safe?
            batch_loss.backward()
            model.loss += batch_loss.item()
            hyper.Opt.step()
        model.TrainLosses.append(model.loss / len(data.train_loader))
        
        # Store the parameters at the current step
        # deepcopy is the only thing that seems to work, TODO: MWE + ask stackoverflow
        # TODO: enhance quantum model into something that inherits from torch.nn.Module, as for the MLP
        if dic['type'] == 'Classical': model.Params.append( deepcopy(model.state_dict()) )
        elif dic['type'] == 'Quantum': model.Params.append( deepcopy(model.params) )

        # Testing
        if dic['type'] == 'Classical': model.eval() # deactivate autograd
        model.loss = 0
        for in_test, out_test in data.test_loader:
            pred = model.forward(in_test)
            batch_loss = hyper.Loss(pred.squeeze(), out_test.squeeze()) #NOTE: is squeezing really safe?
            model.loss += batch_loss.item()
        model.TestLosses.append(model.loss / len(data.test_loader))

        # early stopping
        if model.TrainLosses[-1] < early_stopping_thr and model.TestLosses[-1] < early_stopping_thr:
            break

    end = tm()
    save(dic, model, hyper, data, [start, end]) # writes out results
    plot(model, data)
    #conv.regenModel(model.path)


def main():
    # TODO: write a slightly lengthy explanation for this abscond loop
    for t in Types:
        print("Training %ss" %t.upper(), end='\n')
        # Create a dict containing the lists of params, generate a dict for a set of values with same keys as the big one
        # This is tedious
        Dic = {**SharedDic, **ClassicalDic} if t=='mlp' else {**SharedDic, **QuantumDic} #if t='qnn' else raise KeyError('Type must be mlp or qnn')
        List = []
        for k, v in Dic.items():
            List.append(v)

        st = tm()
        # Progress bar-friendly loop.
        # Caveat: regen Prod at each iter, but not a big deal. Can be slow in case if large nbr of batches and few models.
        # Cool side effect: avoids misschedules of joblib
        for idx in tqdm(range(Passes), desc='Batches', position=0):
            Prod = product(*List) #product of all possible configurations
            Parallel(n_jobs=nprocs)(delayed(loopModel)(paramSet, Dic, idx) for paramSet in Prod)
        
        #Parallel(n_jobs=nprocs)(tqdm(delayed(loopModel)(paramSet, Dic, idx) for paramSet in ProdParallel for idx in range(Passes)))
        
        print('parallel', tm()-st)

if __name__ == "__main__":
    s = tm()
    main()
    print(tm() - s)

