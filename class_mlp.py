import torch

# This class builds a classical multi-layer perceptron
class Mlp(torch.nn.Module): #nn building happens here
    # largely based on Fred's code and the pytorch post 13124/5
    def __init__(self, input_size, layers_params: list):
        super(Mlp, self).__init__() # inherit from torch.nn.Module

        self.layers = torch.nn.ModuleList()
        self.input_size = input_size

        # Create the network
        for size, activation in layers_params:
            self.layers.append(torch.nn.Linear(input_size, size))
            input_size = size # input for the next layer
            if activation is not None:
                assert isinstance(activation, torch.nn.Module), "Each layer_param should contain a size and a torch.nn.modules.Module"
                self.layers.append(activation)

        self.device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        self.to(self.device)


        # Losses and parameters
        self.loss = 0 # FIXME: should it be there?
        self.TrainLosses = []
        self.TestLosses  = []
        self.Params      = []

        # Useful info: mimic what's done for qnn
        self.type = 'mlp'
        self.nb_neurons_list = [size for size, act in layers_params]
        self.nb_neurons = sum(self.nb_neurons_list)
        self.nb_params_list = [p.numel() for p in self.parameters() if p.requires_grad]
        self.nb_params = sum(self.nb_params_list)
        #self.nb_params = sum(p.numel() for name, p in self.named_parameters() if p.requires_grad)
        #self.nb_params = sum(dict((p.data_ptr(), p.numel()) for p in self.parameters()).values())

    # Forward pass
    def forward(self, input_params):
        for layer in self.layers:
            input_params = layer(input_params)
        return input_params

    # Convenience function to generate a list of (neurons, activ)
    # The last layer automatically has dim_output neurons
    # FIXME: should this even be a member of Mlp?
    #def gen_archi(nb_layers, act, nb_neurons, dim_output, geom):
    #    #act = torch.nn.ReLU() #TODO: make this an input
    #
    #    Geom = make_scaled_geom(nb_layers-1, nb_neurons, geom)
    #    layer_params = [(layer, act) for layer in geom]
    #    #layer_params.append[(dim_output, torch.nn.Sigmoid())] #NOTE: use an activation that's consistent with data normalisation, e.g. NOT SIGMOID FOR DATA IN [-1;1]
    #    layer_params.append[(dim_output, torch.nn.Tanh())]

    #    return layer_params

    # Set the path to the location of this model's outputs
    def set_path(self, path):
        self.path = path

    
    # Convenience geometry-generating functions
    # Create a list of neurons numbers corresponding to a given shape. neuron_min is used only for auto-encoding-type and diamond shapes
    #def make_geom(nb_layers, neuron_max, neuron_min=1, shape='funnel'):
    #    divide = lambda n: int(np.ceil(n*0.5)) #shorthand
    #    # TODO: switch for pointy triangles (n_out=1) or not
    #    layers = []
    #    if shape == 'funnel':
    #        layers.append(neuron_max)
    #        [layers.append( divide(layers[k-1]) ) for k in range(1, nb_layers)]

    #    if shape == 'long_funnel':
    #        half = int(np.ceil(nb_layers * 0.5))
    #        layers = [neuron_max for k in range(half)]
    #        [layers.append( divide(layers[k-1]) ) for k in range(half, nb_layers)]

    #    if shape == 'brick':
    #        [layers.append(neuron_max) for k in range(nb_layers)]

    #    if shape == 'triangle':
    #        layers = Shape(nb_layers, neuron_max, shape='funnel')[::-1] #mastermind move

    #    if shape == 'rhombus':
    #        c = (neuron_max - 1) / (np.ceil(nb_layers/2) - 1)
    #        top = [int(np.ceil(c*i + 1)) for i in range(int(nb_layers/2))]
    #        bot = top[::-1]
    #        if nb_layers % 2:
    #            top.append(neuron_max)
    #        layers = top + bot

    #    if shape == 'diamond': #different from Talos'
    #        waist = neuron_min
    #        top = Shape(waist+1, neuron_max, shape='triangle')[:-1]
    #        bot = Shape(nb_layers-waist, neuron_max, shape='funnel')
    #        layers = top + bot

    #    if shape == 'hexagon': #different from Talos'
    #        top = Shape(int(np.ceil(nb_layers/3)), neuron_max, shape='funnel')[::-1]
    #        bot = Shape(int(np.ceil(nb_layers/3)), neuron_max, shape='funnel')
    #        while len(top+bot)<nb_layers:
    #            top.append(neuron_max)
    #        layers = top + bot
    #   # Auto-encoding-suited geometries
    #    if shape == 'hourglass':
    #        top = Shape(int(nb_layers/2), neuron_max, shape='funnel')
    #        bot = Shape(int(nb_layers/2), neuron_max, shape='triangle')
    #        if nb_layers % 2:
    #            top.append(neuron_min)
    #        else:
    #            top[-1], bot[0] = neuron_min, neuron_min
    #        layers = top + bot

    #    if shape == 'long_hourglass':
    #        top = Shape(int(nb_layers/2), neuron_max, shape='long_funnel')
    #        bot = Shape(int(nb_layers/2), neuron_max, shape='long_funnel')[::-1]
    #        if nb_layers % 2:
    #            top.append(neuron_min)
    #        else:
    #            top[-1], bot[0] = neuron_min, neuron_min

    #        layers = top + bot

    #    # final clamping of the neuron numbers
    #    l      = [e if e>=neuron_min else neuron_min for e in layers]
    #    layers = [e if e<=neuron_max else neuron_max for e in l]

    #    assert(len(layers) == nb_layers)
    #    assert(np.max(layers) == neuron_max)
    #    return layers

    #    # Construct a geometry with number of neurons closest to target, fixed nb_layers
    #    def make_scaled_geom(nb_layers, nb_neurons, geom):
    #        Min, Max = 1, nb_neurons
    #        TotMax = np.sum(make_geom(nb_layers, nb_neurons, shape=geom))
    #        TotMin = np.sum(make_geom(nb_layers, 1, shape=geom))

    #        Nbr = nb_neurons
    #        a,i = nb_neurons, 0
    #        while i<a:
    #            Nbr = int((TotMax+TotMin)/2)
    #            Tot = np.sum(make_geom(nb_layers, Nbr, shape=geom))
    #            if Tot > nb_neurons: TotMax = Nbr
    #            else: TotMin = Nbr
    #            i+=1
    #        return make_geom(nb_layers, Nbr, shape=geom)


