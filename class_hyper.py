# Class storing the hyperparameters that will be used to create the model:
# - optimiser
# - loss function
import torch

class Hyper:
    def __init__(self, epochs, opt, loss, params):
        self.epochs = epochs
        self.optName = opt
        self.lossName = loss
        self.Opt = self.init_opt(opt.lower(), params)
        self.Loss = self.init_loss(loss.lower())

    # Fetch and init a Torch optimiser
    def init_opt(self, opt, params):
        if   opt=='adadelta':   o=torch.optim.Adadelta(params)
        elif opt=='adagrad':    o=torch.optim.Adagrad(params)
        elif opt=='adam':       o=torch.optim.Adam(params, lr=0.01)
        elif opt=='adamw':      o=torch.optim.AdamW(params)
        elif opt=='sparseadam': o=torch.optim.SparseAdam(params)
        elif opt=='adamax':     o=torch.optim.Adamax(params)
        elif opt=='asgd':       o=torch.optim.ASGD(params)
        elif opt=='lbfgs':      o=torch.optim.LBFGS(params)
        elif opt=='nadam':      o=torch.optim.NAdam(params)
        elif opt=='radam':      o=torch.optim.RAdam(params)
        elif opt=='rmsprop':    o=torch.optim.RMSprop(params)
        elif opt=='rprop':      o=torch.optim.Rprop(params)
        elif opt=='sgd':        o=torch.optim.SGD(params)
        else: return ("Optimiser name %s not understood." %opt)
        return o
    
    #def customloss(self, y, yhat):
    #    return torch.sum((y-yhat)**2)
    #    #return torch.nn.MSELoss(yhat, y)
    # Fetch a Torch loss function
    def init_loss(self, loss):
        if   loss=='l1':                   l=torch.nn.L1Loss()
        #elif loss=='custom':                  l=self.customloss()
        elif loss=='mse':                  l=torch.nn.MSELoss()
        elif loss=='crossentropy':         l=torch.nn.CrossEntropyLoss()
        elif loss=='ctc':                  l=torch.nn.CTCLoss()
        elif loss=='nll':                  l=torch.nn.NLLLoss()
        elif loss=='poissonll':            l=torch.nn.PoissonNLLLoss()
        elif loss=='gaussianll':           l=torch.nn.GaussianNLLLoss()
        elif loss=='kldiv':                l=torch.nn.KLDivLoss()
        elif loss=='bce':                  l=torch.nn.BCELoss()
        elif loss=='bcelogist':            l=torch.nn.BCEWithLogistLoss()
        elif loss=='marginranking':        l=torch.nn.MarginRankingLoss()
        elif loss=='hingeembedding':       l=torch.nn.HingeEmbeddingLoss()
        elif loss=='multilabelmargin':     l=torch.nn.MultiLabelMarginLoss()
        elif loss=='huber':                l=torch.nn.HuberLoss()
        elif loss=='smoothl1':             l=torch.nn.SmoothL1Loss()
        elif loss=='softmargin':           l=torch.nn.SoftMarginLoss()
        elif loss=='multilabelsoftmargin': l=torch.nn.MultiLabelSoftMarginLoss()
        elif loss=='cosineembedding':      l=torch.nn.CosineEmbeddingLoss()
        elif loss=='multimargin':          l=torch.nn.MultiMarginLoss()
        elif loss=='tripletmargin':        l=torch.nn.TripletMarginLoss()
        elif loss=='tripletmarginwithdistance': l=torch.nn.TripletMarginWithDistanceLoss()
        else: return ("Loss function name %s not understood." %loss)
        return l



