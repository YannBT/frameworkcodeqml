'''
This file is aimed at being a helper for installing all the python libraries required by the code.
It lists all the potentially required packages, and tries to install them using pip3.
For each module, a confirmation is asked for. 
This script is not meant to provide a "keys in hand" installation, but rather a helper to avoid the tedious finding of all the needed modules' names.
Hopefully, no weird things (e.g. installation of unwanted packages, non-installation of required ones, etc) should occur.
Please report any bug to beaujeaul@ijclab.in2p3.fr, and feel free to tweak this file to your needs.
'''

from glob import glob
import os

# Ask for the package manager
installer = input('Which package manager do you want to use? (pip3 works fine) ')

# List all python files where a line contains an import
os.system('grep -r \'import\' *.py > well_named_file.txt')

# List all python files. This to avoid (as much as possible) trying to install modules thinking they are libraries.
files = [f for f in glob('*.py')]

# Read the file and clean it up, keeping only the names of the modules
modulesNames = []
for line in open('well_named_file.txt'):
    # Avoid this file
    if line.find('install_required_modules.py') >= 0:
        continue
   
    # Find the name of the python file corresponding to the current line
    py = line[:line.find('.py')+3]

    # Name of the library or .py file from which we try to import
    importingFrom = line.find('from')

    if importingFrom > 0:
        l = line[line.find('from'):]
        l = l.split()
        try: 
            moduleName = l[1]
        except:
            print('Could\'nt understand the module %s from %s' %(l,py))
        modulesNames.append(moduleName)

# Quick cleanup
modulesNames = [m for m in modulesNames if m+'.py' not in files]# Remove entries that match a *.py file
modulesNames = [m for m in modulesNames if '.' not in m]# Remove entries that contain a '.', since this indicates a submodule
modulesNames = list(set(modulesNames)) # Remove duplicates

# Prompt for confirmation
def confirm(mod, modulesNames, kept):
    conf = input('Install %s? [Y/n] ' %mod)
    if conf == 'n':
        return kept
    elif conf == 'Y':
        kept.append(mod)
        return kept
    else:
        print('I did not understand...')
        return confirm(mod, modulesNames, kept)


print('Need confirmation to install up to %s modules' %len(modulesNames))
kept = [] # list of modules that you accept installing
for mod in modulesNames:
    # Ask for confirmation for all the modules
    kept = confirm(mod, modulesNames, kept) 


success, fail = [], []

for mod in kept:
    try:
        os.system(installer + ' install ' + mod)
        success.append(mod)
    except: 
        print('Could not install ' + mod)
        fail.append(mod)

print('The following models seem to have been installed: %s' %success)
print('The following models seem not to have been installed: %s' %fail)
os.remove('well_named_file.txt')
