# Generate .h5 files for the 1d challenges
# for each challenge, generate (i) a file with {input, output}, so that the sizes of the training and testing sets can be chosen by the user, (ii) a file with fixed {input_train, output_train, input_test, output_test} for reproducibility
# TODO: exhaust all the possibilities for the data generation (rand vs grid vs split vs whatever...)

import sys
import numpy as np
import h5py
import ch1d, ch2d
import challenges
import matplotlib.pyplot as plt # for visual checks of the generated data

n = 30 # number of training points
N = 100 # 'exact grid' nbr of points


print(sys.argv)
if len(sys.argv)<2:
        print("usage: %s challenge_name"%(sys.argv[0]))
        sys.exit(1)
name=sys.argv[1]

func = None
# One dimensional functions
if   name=='crenel1':  func = ch1d.crenel
elif name=='expFred':  func = ch1d.expFred
elif name=='gauss1':   func = ch1d.gauss
elif name=='poly_PS1': func = ch1d.poly_PS
elif name=='reLU1':    func = ch1d.reLU
elif name=='reLUreg1': func = ch1d.reLUreg
elif name=='sigm1':    func = ch1d.sigm
elif name=='sin1':     func = ch1d.sin
elif name=='step1':    func = ch1d.step
elif name=='tanh1':    func = ch1d.tanh
# Two dimensional functions
elif name=='crenel2':  func = ch2d.crenel
elif name=='expFred2': func = ch2d.expFred
elif name=='gauss2':   func = ch2d.gauss
elif name=='poly_PS2': func = ch2d.poly_PS
elif name=='reLU2':    func = ch2d.reLU
elif name=='reLUreg2': func = ch2d.reLUreg
elif name=='sigm2':    func = ch2d.sigm
elif name=='sin2':     func = ch2d.sin
elif name=='step2':    func = ch2d.step
elif name=='tanh2':    func = ch2d.tanh
else:
    print("function %s not found"%name)
    sys.exit(1)

# Generate data
def genFred(func, name):
    # name is used only to determine the nbr of dimensions
    isOne = '1' in name
    isTwo = '2' in name
    isThr = '3' in name
    dim = 0
    if isOne: dim = 1
    if isTwo: dim = 2
    if isThr: dim = 3
    
    # assert the dim has been unambiguously read
    assert([isOne, isTwo, isThr].count(True) == 1)
    
    # generate input and output data
    x,y,z = np.random.rand(n), np.random.rand(n), np.random.rand(n)
    x,y,z = 2 * x - 1, 2*y-1, 2*z-1
    
    xgrid = np.linspace(-1,1, N) #for plots here and in the QML code
    ygrid = xgrid
    zgrid = xgrid

    f, fgrid = None, None
    if dim == 1:
        f = func(x)
        fgrid = func(xgrid)
    elif dim == 2: 
        f = func(x,y) #do not eval on meshgrid here!
        xgrid, ygrid = np.meshgrid(xgrid,ygrid)
        fgrid = func(xgrid,ygrid) #!!! meshgrid messes up the evaluated funcs
    elif dim == 3: 
        f = func(x,y,z) #do not eval on meshgrid here!
        xgrid, ygrid, zgrid = np.meshgrid(xgrid,ygrid, zgrid)
        fgrid = func(xgrid,ygrid,zgrid)

    # plots for visual checks
    import matplotlib.pyplot as plt
    if dim == 1:
        plt.plot(xgrid, fgrid)
        plt.plot(x, f, ls='', marker='x', color='k', label='sampled')
        plt.xlabel('x')
        plt.ylabel('f(x)')
        plt.legend()
        plt.title('f='+name)
        plt.savefig(name + '.pdf')
    if dim == 2:
        fig, ax = plt.subplots()
        plt.imshow(fgrid, extent=[-1,1,-1,1])
        plt.scatter(x, y, marker='x', color='k', label='sampled')
        plt.xlabel('x')
        plt.ylabel('y')
        plt.colorbar()
        plt.title('f='+name) #TODO: not optimal, e.g. sin2 while sin(x+y) is better
        plt.savefig(name + '.pdf')

    # tranform to lists to remove ambiguities (no ambiguities if nbr of dims is given, but this to have a well-defined structure)
    newx = [[val] for val in x]    
    newf = [[val] for val in f]
    newxgrid = [[val] for val in xgrid]
    newfgrid = [[val] for val in fgrid]

    challenges.save_challenge(newx, newf, name+'_mm')
    challenges.save_challenge(newxgrid, newfgrid, name+'_mm_grid')

genFred(func, name)
