import numpy as np
import ch1d # this to define some functions systematically from their 1d equivalent

# This file contains several two-dimensional functions that are useful and/or interesting for studying the performances of QML circuits

# Most are very simple and don't require to be declared in this specific file, but this is done to ensure exhaustivity and modularity

def crenel(x,y): 
    return ch1d.crenel(x) * ch1d.crenel(y)
def expFred(x,y):
    return ch1d.expFred(x) * ch1d.expFred(y)
def gauss(x, y, mx=0, my=0, sx=1, sy=1): return np.exp(-((x-mx)**2 / (2*sx**2) + (y-my)**2 / (2*sy**2)) )

def poly_PS(x,y): #7th order polynomial of Perez-Salinas et al., https://arxiv.org/pdf/2102.04032.pdf 
    return ch1d.polyPS(x) * ch1d.ployPS(y)
def reLU(x, y): 
    return x * np.heaviside(x, 0.0) * y * np.heaviside(y, 0.0)
def sigm(x,y,la=1): 
    return ch1d.sigm(x) * ch1d.sigm(y)
def sin(x, y, la=np.pi): 
    return np.sin(la * (x+y))
def step(x,y): 
    return ch1d.step(x) * ch1d.step(y)#np.heaviside(x, 0.0) * np.heaviside(y, 0.0)
def tanh(x,y): 
    return ch1d.tanh(x) * ch1d.tanh(y)
