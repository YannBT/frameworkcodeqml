# Generate .h5 files for the 1d challenges
# for each challenge, generate (i) a file with {input, output}, so that the sizes of the training and testing sets can be chosen by the user, (ii) a file with fixed {input_train, output_train, input_test, output_test} for reproducibility
# TODO: exhaust all the possibilities for the data generation (rand vs grid vs split vs whatever...)

import sys
import numpy as np
import h5py
import ch1d

print(sys.argv)
if len(sys.argv)<2:
        print("usage: %s challenge_name"%(sys.argv[0]))
        sys.exit(1)
name=sys.argv[1]

func = None
if   name=='crenel':  func = ch1d.crenel
elif name=='expFred': func = ch1d.expFred
elif name=='gauss':   func = ch1d.gauss
elif name=='poly_PS': func = ch1d.poly_PS
elif name=='reLU':    func = ch1d.reLU
elif name=='reLUreg': func = ch1d.reLUreg
elif name=='sigm':    func = ch1d.sigm
elif name=='sin':     func = ch1d.sin
elif name=='step':    func = ch1d.step
elif name=='tanh':    func = ch1d.tanh
else:
    print("function %s not found"%name)
    sys.exit(1)

x = np.linspace(-np.pi, np.pi, 100)
if name=='expFred': x = np.linspace(-4,10,100)

def gen(func, seedTrain=None, seeTest=None):
    input_train = np.random.rand(10) * (x[1]-x[0]) + x[0]
    output_train = func(input_train)

    # train = random, test = grid
    f1 = h5py.File(name + '_1.h5', 'w')
    f1.create_dataset('input_train',  data=input_train,  dtype=float)
    f1.create_dataset('input_test',   data=x,            dtype=float)
    f1.create_dataset('output_train', data=output_train, dtype=float) 
    f1.create_dataset('output_test',  data=func(x),      dtype=float)
    f1.close()

    # train = fixed rand, test = grid
    np.random.seed(123456789)
    input_train = np.random.rand(10)
    np.random.seed()
    output_train = func(input_train)
    f2 = h5py.File(name + '_2.h5', 'w')
    f2.create_dataset('input_train',  data=input_train,  dtype=float)
    f2.create_dataset('input_test',   data=x,          dtype=float)
    f2.create_dataset('output_train', data=output_train, dtype=float) 
    f2.create_dataset('output_test',  data=func(x),      dtype=float)
    f2.close()
   
    # train & test random, not split
    np.random.seed(987654321)
    xx = np.random.rand(100) * (x[1]-x[0]) + x[0]
    np.random.seed()
    ff = func(xx)
    f3 = h5py.File(name + '_3.h5', 'w')
    f3.create_dataset('input',  data=xx, dtype=float)
    f3.create_dataset('output', data=ff, dtype=float)
    f3.close()

   
    fread = h5py.File(name + '_3.h5', 'r')
    fread['input']

#gen(func)



def newgen(func, train_mode, test_mode):
    input_train, input_test, output_train, output_test = [], [], [], []

    if train_mode == 'rand':
        input_train = np.random.rand(10) * (x[-1]-x[0]) + x[0]
    elif train_mode == 'seeded':
        np.random.seed(123456789)
        input_train = np.random.rand(10) * (x[-1]-x[0]) + x[0]
    elif train_mode == 'grid':
        input_train = np.linspace(x[0], x[-1], 10)
    
    if train_mode == 'rand':
        output_train = np.random.rand(10) * (x[-1]-x[0]) + x[0]
    elif train_mode == 'seeded':
        np.random.seed(123456789)
        output_train = np.random.rand(10) * (x[-1]-x[0]) + x[0]
    elif train_mode == 'grid':
        output_train = np.linspace(x[0], x[-1], 10)
   
    modes = train_mode[0]+test_mode[0]


    # train = random, test = grid
    f1 = h5py.File(name + '_' + modes + '.h5', 'w')
    f1.create_dataset('input_train',  data=input_train,  dtype=float)
    f1.create_dataset('input_test',   data=input_test,   dtype=float)
    f1.create_dataset('output_train', data=output_train, dtype=float) 
    f1.create_dataset('output_test',  data=output_test,      dtype=float)
    f1.close()
    
    fread = h5py.File(name + '_rr.h5', 'r')
    fread['input_train']

for train in ['random', 'seeded', 'grid']:
    for test in ['random', 'seeded', 'grid']:
        newgen(func, train, test)


