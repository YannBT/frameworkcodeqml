import os
import sys
import glob

funcs = ['crenel', 'expFred', 'gauss', 'poly_PS', 'reLU', 'reLUreg', 'sigm', 'sin', 'step', 'tanh'] 

print(sys.argv)
if len(sys.argv)<2:
        print("usage: %s regen"%(sys.argv[0]))
        sys.exit(1)
regen=sys.argv[1].lower()
try: conf =sys.argv[2].lower()
except: conf=[]
print(conf)

if regen=='true':
    valid = conf
    print('All data is about to be regen. Are you sure?')
    if valid == []:
        valid = input('Regenerate all data? ')
    if valid.lower() not in ['y', 'yes']:
        print('Exiting')
        sys.exit(1)

os.system('wc -l *.py')
for func in funcs:
    if regen=='true':
        os.system('rm -f ' + func + '*.h5')
    else:
        if len([f for f in glob.glob(func + '_*.h5')]) != 9: #FIXME: seems dangerous to have a nbr hard-written
            print('Missing data for %s, regen it' %func)
            os.system('rm -f ' + func + '*.h5')

    os.system('python3 generation.py ' + func)

