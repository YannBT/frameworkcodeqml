#!/usr/bin/env python3
# coding: utf-8

import torch

class mlp(torch.nn.Module):
        def __init__(self,dims):
                super(mlp, self).__init__()
                self.relu=torch.nn.ReLU()
                self.sigmoid=torch.nn.Sigmoid()
                self.fc1=torch.nn.Linear(dims[0],dims[1])
                self.fc2=torch.nn.Linear(dims[1],dims[2])
                self.fc3=torch.nn.Linear(dims[2],dims[3])
                self.fc4=torch.nn.Linear(dims[3],dims[4])
                self.fc5=torch.nn.Linear(dims[4],1)

        def forward(self,input):
                x=self.relu(self.fc1(input))
                x=self.relu(self.fc2(x))
                x=self.relu(self.fc3(x))
                x=self.relu(self.fc4(x))
                x=self.sigmoid(self.fc5(x))
                return x

dims=[2,500,250,100,50]
nb_epoch=1000
