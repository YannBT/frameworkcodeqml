#!/usr/bin/env python3
# coding: utf-8

import sys
from challenges import *
import numpy as np
import torch
from torch.utils import data
import matplotlib.pyplot as plt

class mlp(torch.nn.Module):
        def __init__(self,dims):
            super(mlp, self).__init__()
            self.relu=torch.nn.ReLU()
            self.sigmoid=torch.nn.Sigmoid()
            self.fc1=torch.nn.Linear(dims[0],dims[1])
            #self.fc2=torch.nn.Linear(dims[1],dims[2])
            self.fc2=torch.nn.Linear(dims[1],1)

        def forward(self,input):
            x=self.relu(self.fc1(input))
            #x=self.relu(self.fc2(x))
            return self.sigmoid(self.fc2(x))
            

class mlpl(torch.nn.Module):
        def __init__(self,dims):
            super(mlp, self).__init__()
            self.fc1=torch.nn.Linear(2,1)

        def forward(self,input):
            x=self.fc1(input)
            return x 

dims=[2,8]
nb_epoch=150


