#!/usr/bin/env python3
# coding: utf-8

import sys
from challenges import *
import numpy as np
import torch
from torch.utils import data
import matplotlib.pyplot as plt
import importlib

print(sys.argv)
if len(sys.argv)<2:
        print("usage : %s challenge_name"%(sys.argv[0]))
        sys.exit(1)
name=sys.argv[1]
chmod=importlib.import_module(name)


x,y,train_loader,test_loader=load_challenge(name)
plot_challenge(x,y,name)
model=chmod.mlp(chmod.dims)
criterion=torch.nn.BCELoss()
optimizer=torch.optim.SGD(model.parameters(),lr=0.01)
pepoch,ptr_loss,pte_loss=train(train_loader,test_loader,model,optimizer,criterion,nb_epoch=chmod.nb_epoch)
torch.save(model,"%s.pth"%(name))
plot_loss(pepoch,ptr_loss,pte_loss)
plot_classif(x,y,model)
grid_search(x,y,model)

