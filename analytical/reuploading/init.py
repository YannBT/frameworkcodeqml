import os

# This should init the repository because I fell it's going to be messy if done within the code

dirs_to_init = ['tex']

for d in dirs_to_init:
    if not os.path.isdir(d):
        os.mkdir('tex')
        print('Folder', d, 'created.')
    else:
        print('Folder', d, 'already exists.')




