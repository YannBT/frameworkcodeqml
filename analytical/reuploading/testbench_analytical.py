import sympy as sy
from sympy.simplify.fu import TR8 #does the magic, see https://docs.sympy.org/latest/modules/simplify/fu.html?highlight=trigonometric#sympy.simplify.fu.TR8
from sympy import cos, sin, exp
from tqdm import tqdm
from time import time as tm
from sympy.physics.quantum import Dagger
sy.init_printing() # allows latex printing

##################### INPUTS #####################
# Sequence of gates.
ng = 4 #number of gates
seq = ['RX' if i%2==0 else 'RY' for i in range(ng)]
print('Sequence of rotations: ', seq) #applied as seq[-1] * ... * seq[0]

# this time with real inputs
def calc_expr(seq):
    ng = len(seq)

##################################################

# Convention
conv = 'pl' # 'pl' or 'qk'

# Set of angles. These are used following Pennylane's convention
# PL->QK amounts to PHI<->TH, OM->LA
PHI = sy.symbols('phi0:%d' %ng,  real=True) #telling sympy these are real is useful for conjugation
TH  = sy.symbols('th0:%d' %ng, real=True)
OM  = sy.symbols('om0:%d' %ng,  real=True)
LA  = sy.symbols('la0:%d' %ng,  real=True)

print(type(PHI))
print(PHI)
print(PHI+LA)

#vec  = sy.Matrix([1,1]) #don't normalise bc it looks ugly 
Gate = sy.Identity(2) # initial identity gate, on which the rotations will be applied
Z = sy.Matrix([[1.0, 0.0], [0.0, -1.0]])

def RX(th):
    mat = sy.MutableDenseNDimArray(range(2*2), (2,2), commutative=False)
    mat[0,0], mat[0,1], mat[1,0], mat[1,1] = cos(th/2), -1j*sin(th/2), -1j*sin(th/2), cos(th/2)
    return sy.Matrix(mat)

def RY(th):
    mat = sy.MutableDenseNDimArray(range(2*2), (2,2), commutative=False)
    mat[0,0], mat[0,1], mat[1,0], mat[1,1] = cos(th/2), -sin(th/2), sin(th/2), cos(th/2)
    return sy.Matrix(mat)

def RZ(th):
    mat = sy.MutableDenseNDimArray(range(2*2), (2,2), commutative=False)
    mat[0,0] = cos(th/2) - 1j*sin(th/2)
    mat[1,1] = cos(th/2) + 1j*sin(th/2)
    return sy.Matrix(mat)

# Generic one qubit rotation
# The generic rotation is different between Qiskit and PennyLane
def U(phi,th,om,la, gate='U', conv='pl'):
    mat = sy.MutableDenseNDimArray(range(2*2), (2,2), commutative=False)

    # U gate, Pennylane
    if conv == 'pl':
        mat[0,0] = +cos(th/2) * (cos((phi+om)/2) - 1j*sin((phi+om)/2))
        mat[0,1] = -sin(th/2) * (cos((phi+om)/2) + 1j*sin((phi-om)/2))
        mat[1,0] = +sin(th/2) * (cos((phi+om)/2) - 1j*sin((phi-om)/2))
        mat[1,1] = +cos(th/2) * (cos((phi+om)/2) + 1j*sin((phi+om)/2))

    # U gate, Qiskit
    if conv == 'qk':
        mat[0,0] = +cos(th/2)
        mat[0,1] = -sin(th/2) * (cos(la)  - 1j*sin(la))
        mat[1,0] = +sin(th/2) * (cos(phi) + 1j*sin(phi))
        mat[1,1] = +cos(th/2) * (cos(phi+la) + 1j*sin(phi+la))

    if conv == 'pl':
        if gate=='RX': mat = RX(phi)
        if gate=='RY': mat = RY(phi)
        if gate=='RZ': mat = RZ(phi)
    
    if conv == 'qk':
        if gate=='RX': mat = RX(th)
        if gate=='RY': mat = RY(th)
        if gate=='RZ': mat = RZ(la)
    return sy.Matrix(mat)

# as a function because we might want more complicated dependencies
def calc_angles(params, data): #should be able to return tensors and what's convenient for the lambdified func
    angles = [params[i] * data + params[i+1] for i in range(0,len(params),2)] #TODO check this wrt PL code
    return angles


########## Test bench
lin_vec = sy.Matrix([1,1]) #will hold the linearised trig functions
lin_Gate = sy.Matrix(sy.Identity(2))
nb_terms, nb_terms_Gate= [], []

# Apply the gates
for i in tqdm(range(ng)): #this is fast and O(ng)
    Gate = U(PHI[i], TH[i], OM[i], LA[i], gate=seq[i]) * Gate

#for i in tqdm(range(2)): #this is O(exp^ng)
    #lin_vec[i] = TR8(vec[i])
    #sy.expand() expands the fractions
    #sy.Add.make_args counts the number of terms in the expanded function
    #nb_terms.append(len(sy.Add.make_args(sy.expand(lin_vec[i])))) 

#TODO: do the simplifications at each gate application rather than at the end
for i in tqdm(range(2)):
    for j in tqdm(range(2)):
        lin_Gate[i,j] = TR8(Gate[i,j])
        nb_terms_Gate.append(len(sy.Add.make_args(lin_Gate[i,j])))

print(nb_terms_Gate)

expval_Z = Dagger(Gate) * Z * Gate

# Applying the circuit to |0> selects the [0,0] component 
# TODO: it might be possible to inspect expval_Z to find which entry is richest
print('Starting simplification')
s = tm()
Zs = sy.simplify(expval_Z)
print('Simplification done in ', tm() - s, ' s')

out = open('tex/output_' + ''.join(seq[::-1]) + '.tex', 'w')
out.write(sy.latex(Zs))
out.close()

out = open('tex/output_' + ''.join(seq[::-1]) + '_00.tex', 'w')
out.write(sy.latex(Zs[0,0]))
out.close()

### self-contained test bench
import numpy as np
import matplotlib.pyplot as plt
#seq = ['RX', 'RY', 'RX']
Gate = sy.Identity(2)
ANGLES = PHI + TH + OM + LA
for g in tqdm(range(len(seq))):
    Gate = U(PHI[g], TH[g], OM[g], LA[g], gate=seq[g]) * Gate
expval_Z = Dagger(Gate) * Z * Gate
Z00 = expval_Z[0,0]

ops = Zs.count_ops(visual=True) # count the number of operations, which are here sines, cosines, sums, prods
print('nbr of ops in simplified exp val:     ', ops)
ops = expval_Z.count_ops(visual=True) # count the number of operations, which are here sines, cosines, sums, prods
print('nbr of ops in non-simplified exp val: ', ops)


dummy_params = [-0.52980859, -0.55160236,  0.45648499,  0.41804285,  0.71599451, -0.7802086 ]
dummy_params = [0.0553, -0.2312, 0.2662, 1.9796, 0.6638, 1.0940]
x = np.linspace(-np.pi, np.pi, 10000)

# the .free_symbols method returns a set, which needs to be ordered
free = list(Z00.free_symbols)
idx = [ANGLES.index(f) for f in free]
idx, free = zip(*sorted(zip(idx, free)))

angles = tuple(free)
#a = calc_angles([1,-np.pi/2,0,0], x)
a = calc_angles(dummy_params, x)
np_expr = sy.lambdify(angles, Zs[0,0])
plt.plot(x, np_expr(*a)) # * unpacks the argument
#plt.plot(x, np.sin(x), ls='--')
plt.show()
#TODO: merge this with a circuit in which calc_angles() is called
# this should be all there is to do



