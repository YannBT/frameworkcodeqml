import sympy as sy
from sympy.simplify.fu import TR8 #does the magic, see https://docs.sympy.org/latest/modules/simplify/fu.html?highlight=trigonometric#sympy.simplify.fu.TR8
from sympy import cos, sin, exp
from tqdm import tqdm
from time import time as tm
from sympy.physics.quantum import Dagger
sy.init_printing() # allows latex printing

##################### INPUTS #####################
# Sequence of gates.
#ng = 4 #number of gates
#seq = ['RX' if i%2==0 else 'RY' for i in range(ng)]
#print('Sequence of rotations: ', seq) #applied as seq[-1] * ... * seq[0]

# CAVEAT: the code has not been checked when using qiskit's convention. It should however work as long as we don't use U gates with this conv

# INPUTS:
#   - conv = 'pl' or 'qk': convention for the generic one-qubit rotation, PennyLane vs Qiskit. Default is pl
#   - op   = 'Z' or 'X' or 'Y': operator of which the expectation value is taken, ie <C\dg op C>, with C the circuit. Default is Z
# - ini & out: the states according to which the measurement is performed, i.e. <out|C\dg op C|ini>. Default is 0 & 0

def calc_expr(seq, op='Z', conv='pl', ini=0, out=0):
    # types checks etc
    assert(type(seq) == list)
    assert(op in ['Z', 'X', 'Y'])
    assert(conv in ['pl', 'qk'])
    assert(ini in [0, 1])
    assert(out in [0, 1])

    ng = len(seq)

    # Set of angles. These are used following Pennylane's convention
    # PL->QK amounts to PHI<->TH, OM->LA
    PHI = sy.symbols('phi0:%d' %ng,  real=True) #telling sympy these are real is useful for conjugation
    TH  = sy.symbols('th0:%d' %ng, real=True)
    OM  = sy.symbols('om0:%d' %ng,  real=True)
    LA  = sy.symbols('la0:%d' %ng,  real=True)
    ANGLES = PHI + TH + OM + LA #for finding the free symbols

    Gate = sy.Identity(2) # initial identity gate, on which the rotations will be applied

    #op = sy.Matrix([[1.0, 0.0], [0.0, -1.0]]) if op == 'Z' else sy.Matrix([[0.0, 1.0], [1.0, 0.0]]) if op == 'X' else sy.Matrix([[0.0, -1.0j], [1.0j, 0.0]]) if op == 'Y'  

    if op == 'Z':  op = sy.Matrix([[1.0, 0.0], [0.0, -1.0]])
    elif op == 'X': op = sy.Matrix([[0.0, 1.0], [1.0, 0.0]])
    elif op == 'Y': op = sy.Matrix([[0.0, -1.0j], [1.0j, 0.0]])

    # Apply the gates
    for i in tqdm(range(ng)): #this is fast and O(ng)
        Gate = U(PHI[i], TH[i], OM[i], LA[i], gate=seq[i]) * Gate

    # Expval of the circuit
    expval = Dagger(Gate) * op * Gate
    t = tm() #monitor the time spend for simplification
    expval_simpl = sy.simplify(expval)
    t = tm() - t
    print('Simplification done in ', t, ' s')

    # Write out tex expressions
    output = open('tex/output_' + ''.join(seq[::-1]) + '.tex', 'w')
    output.write(sy.latex(expval_simpl))
    output.close()

    for i in range(2):
        for j in range(2):
            output = open('tex/output_' + ''.join(seq[::-1]) + '_' + str(i) + str(j) + '00.tex', 'w')
            output.write(sy.latex(expval_simpl[i,j]))
            output.close()
    
    # Turn the expression into a callable function
    val = expval_simpl[out,ini]
    free = list(val.free_symbols)
    idx = [ANGLES.index(f) for f in free]
    idx, free = zip(*sorted(zip(idx, free))) # order free correctly (it is a set, and thus unordered)
    
    angles = tuple(free)
    np_expr = sy.lambdify(angles, val)
    
    return np_expr
##################################################

def RX(th):
    mat = sy.MutableDenseNDimArray(range(2*2), (2,2), commutative=False)
    mat[0,0], mat[0,1], mat[1,0], mat[1,1] = cos(th/2), -1j*sin(th/2), -1j*sin(th/2), cos(th/2)
    return sy.Matrix(mat)

def RY(th):
    mat = sy.MutableDenseNDimArray(range(2*2), (2,2), commutative=False)
    mat[0,0], mat[0,1], mat[1,0], mat[1,1] = cos(th/2), -sin(th/2), sin(th/2), cos(th/2)
    return sy.Matrix(mat)

def RZ(th):
    mat = sy.MutableDenseNDimArray(range(2*2), (2,2), commutative=False)
    mat[0,0] = cos(th/2) - 1j*sin(th/2)
    mat[1,1] = cos(th/2) + 1j*sin(th/2)
    return sy.Matrix(mat)

# Generic one qubit rotation
# The generic rotation is different between Qiskit and PennyLane
def U(phi,th,om,la, gate='U', conv='pl'):
    mat = sy.MutableDenseNDimArray(range(2*2), (2,2), commutative=False)

    # U gate, Pennylane
    if conv == 'pl':
        mat[0,0] = +cos(th/2) * (cos((phi+om)/2) - 1j*sin((phi+om)/2))
        mat[0,1] = -sin(th/2) * (cos((phi+om)/2) + 1j*sin((phi-om)/2))
        mat[1,0] = +sin(th/2) * (cos((phi+om)/2) - 1j*sin((phi-om)/2))
        mat[1,1] = +cos(th/2) * (cos((phi+om)/2) + 1j*sin((phi+om)/2))

    # U gate, Qiskit
    if conv == 'qk':
        mat[0,0] = +cos(th/2)
        mat[0,1] = -sin(th/2) * (cos(la)  - 1j*sin(la))
        mat[1,0] = +sin(th/2) * (cos(phi) + 1j*sin(phi))
        mat[1,1] = +cos(th/2) * (cos(phi+la) + 1j*sin(phi+la))

    if conv == 'pl':
        if gate=='RX': mat = RX(phi)
        if gate=='RY': mat = RY(phi)
        if gate=='RZ': mat = RZ(phi)
    
    if conv == 'qk':
        if gate=='RX': mat = RX(th)
        if gate=='RY': mat = RY(th)
        if gate=='RZ': mat = RZ(la)
    return sy.Matrix(mat)

# as a function because we might want more complicated dependencies
def calc_angles(params, data): #should be able to return tensors and what's convenient for the lambdified func
    angles = [params[i] * data + params[i+1] for i in range(0,len(params),2)] #TODO check this wrt PL code
    return angles


########## Test bench

#ops = Zs.count_ops(visual=True) # count the number of operations, which are here sines, cosines, sums, prods
#print('nbr of ops in simplified exp val:     ', ops)
#ops = expval_Z.count_ops(visual=True) # count the number of operations, which are here sines, cosines, sums, prods
#print('nbr of ops in non-simplified exp val: ', ops)
#
#
#dummy_params = [-0.52980859, -0.55160236,  0.45648499,  0.41804285,  0.71599451, -0.7802086 ]
#dummy_params = [0.0553, -0.2312, 0.2662, 1.9796, 0.6638, 1.0940]
#x = np.linspace(-np.pi, np.pi, 10000)

# the .free_symbols method returns a set, which needs to be ordered

########plt.plot(x, np_expr(*a)) # * unpacks the argument
#TODO: merge this with a circuit in which calc_angles() is called
# this should be all there is to do



