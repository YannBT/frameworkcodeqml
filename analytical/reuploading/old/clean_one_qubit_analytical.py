# Given a one-qubit Pennylane circuit C, return the analytical expression of the expectation value of the Pauli gate Z w.r.t that circuit, assuming the qubit is initially in the state |0>
# i.e. return a function f(args) = <0|C^\dagger Z C |0>,
# which can be evaluated like a usual function.
# This provides a fast alternative to sending data through the circuit.

import sympy as sy
from sympy.simplify.fu import TR8 #does the magic, see https://docs.sympy.org/latest/modules/simplify/fu.html?highlight=trigonometric#sympy.simplify.fu.TR8
from sympy import cos, sin, exp, pi
from sympy import Matrix, I
from sympy import pprint
from tqdm import tqdm
from time import time as tm
from sympy.physics.quantum import Dagger
import numpy as np
import matplotlib.pyplot as plt

dict = {'RX': 1, 'RY': 1, 'RZ': 1, 'Rot': 3} # nbr of angles needed for each rotation

def u(angles: list, gate: str):
    if len(angles) != dict[gate]:
        raise KeyError('Wrong number of angles w.r.t given gate: ', gate, '\t', angles)
    assert(type(angles)==list) #FIXME: verbose error msgs (as above) or assertions everywhere?

    # Pad the angles to cast them in a generic one-qubit gate
    if gate=='RX':
        angles.insert(0,pi/2) # pos, val
        angles.insert(2,-pi/2)
    elif gate=='RY':
        angles.insert(0,0) # pos, val
        angles.insert(2,0)
    elif gate=='RZ':
        angles.insert(1,0)
        angles.insert(2,0)
    
    # One-qubit rotation
    mat = sy.MutableDenseNDimArray(range(2*2), (2,2), commutative=False)
    phi, th, om = angles[0]/2, angles[1]/2, angles[2]/2
    mat[0,0] = exp(-I * (phi+om)) * cos(th)
    mat[0,1] = -exp(+I * (phi-om)) * sin(th) 
    mat[1,0] = exp(-I * (phi-om)) * sin(th) 
    mat[1,1] = exp(+I * (phi+om)) * cos(th) 

    return sy.Matrix(mat)

def build_seq(block, nb_gates): #TODO: make this oblivious of the circuit
    #FIXME assuming sequence of RX/RY/RZ rotations only for now
    
    # Infer the rotations from the string 'block'
    seq    = []
    angles = [] # number of angles each rotation takes
    while len(block) > 0:
        print(block)
        if block[:2] == 'RX':
            seq.append('RX')
            block = block[2:]
            angles.append(1)
        elif block[:2] == 'RY':
            seq.append('RY')
            block = block[2:]
            angles.append(1)
        elif block[:2] == 'RZ':
            seq.append('RZ')
            block = block[2:]
            angles.append(1)
        elif block[:3] == 'Rot':
            seq.append('Rot')
            block = block[3:]
            angles.append(3)
        else:
            raise ValueError('Gate not recognised! ', block)

    # Construct the full sequence of rotations
    g = int(np.ceil(nb_gates / len(seq)))
    Seq, Ang = [], []
    for i in range(g):
        Seq += seq
        Ang += angles
    return Seq[:nb_gates], Ang[:nb_gates]

def calc_angles(params, x): #this as a function because we may want other forms
    angles = [params[i] * x + params[i+1] for i in range(0, len(params), 2)]
    return angles

# test bench
block, nb = 'RXRY', 1
Seq, angles_nbrs = build_seq(block, nb)
Seq, angles_nbrs = Seq[::-1], angles_nbrs[::-1] # reverse to apply in the right order
G = sy.Identity(2)

Angles_nbr = np.sum(angles_nbrs)
Angles = sy.symbols('al:0%d' % Angles_nbr, real=True) #FIXME: can I relax the real assumption?
angles_done = 0
for g in range(len(Seq)):
    G = u(list(Angles[angles_done:angles_done+angles_nbrs[g]]), gate=Seq[g]) * G
    angles_done += angles_nbrs[g]
sy.pprint(G) # matrix encoding the formal expr

expVals_Z = Dagger(G) * sy.Matrix([[1.0, 0.0], [0.0, -1.0]]) * G # <Z>
expval_Z = expVals_Z[0,0] # for an initial state |0>

sy.pprint(expval_Z)
expr = sy.simplify(expval_Z) #final expression
np_expr = sy.lambdify(Angles, expr)

x = np.linspace(-np.pi, np.pi, 1000)
plt.plot(x, np.sin(x))
plt.plot(x, np_expr(x-np.pi/2), ls='--')
plt.show()
#res  = expr.evalf(subs = {Angles[i]: np.array([[i+1],[i+1]]) for i in range(len(Angles))})
#print('expr= ', expr)
#print('res= ', res)

a = sy.Symbol('a')
b = sy.Symbol('b')
c = sy.Symbol('c')
print(u([0,0,0], 'Rot'))
print(u([a,0,0], 'Rot'))
print(u([a,b,c], 'Rot'))
raise ValueError()




##################################################################################################
ng = 4 #number of gates
seq = ['RY' if i%2==0 else 'RX' for i in range(ng)]
#seq = ['RY', 'RX'][::-1] # reverse to write in a (hopefully) natural order


TH  = sy.symbols('th0:%d' %ng,  real=True) #telling sympy these are real is useful for conjugation
PHI = sy.symbols('phi0:%d' %ng, real=True)
LA  = sy.symbols('la0:%d' %ng,  real=True)

vec  = sy.Matrix([1,1]) #don't normalise bc it looks ugly 
Gate = sy.Identity(2)

for i in tqdm(range(ng)): #this is fast and O(ng)
    vec  = U(TH[i], PHI[i], LA[i], gate=seq[i]) * vec
    Gate = U(TH[i], PHI[i], LA[i], gate=seq[i]) * Gate

lin_vec = sy.Matrix([1,1]) #will hold the linearised trig functions
lin_Gate = sy.Matrix(sy.Identity(2))
nb_terms, nb_terms_Gate= [], []

for i in tqdm(range(2)): #this is O(exp^ng)
    lin_vec[i] = TR8(vec[i])
    #sy.expand() expands the fractions
    #sy.Add.make_args counts the number of terms in the expanded function
    nb_terms.append(len(sy.Add.make_args(sy.expand(lin_vec[i])))) 

#TODO: do the simplifications at each gate application rather than at the end
for i in tqdm(range(2)):
    for j in tqdm(range(2)):
        lin_Gate[i,j] = TR8(Gate[i,j])
        nb_terms_Gate.append(len(sy.Add.make_args(lin_Gate[i,j])))

print(nb_terms_Gate)

#print(Gate[0,0])
#print(sy.conjugate(TR8(Gate[0,0])))
#print(TR8(Gate[0,0]))
#total = sy.conjugate(sy.transpose(lin_Gate)) * lin_Gate #to check unitarity
#print(total)
w = sy.expand(lin_Gate[0,:]) #action on |0>
print()
#print((sy.expand(TR8(Gate[0,1]))))

expval_Z = Dagger(Gate) * sy.Matrix([[1.0, 0.0], [0.0, -1.0]]) * Gate

# Applying the circuit to |0> selects the [0,0] component 
# TODO: it might be possible to inspect expval_Z to find which entry is richest
Z0  = expval_Z[0,0]
#Z0s = sy.simplify(Z0)
print('Starting simplification')
s = tm()
Z0s = sy.simplify(Z0)
print('Simplification done in ', tm() - s, ' s')

print(Z0s)
ops = Z0s.count_ops(visual=True) # count the number of operations, which are here sines, cosines, sums, prods
print(ops)


