import sympy as sy
from sympy.simplify.fu import TR8 #does the magic, see https://docs.sympy.org/latest/modules/simplify/fu.html?highlight=trigonometric#sympy.simplify.fu.TR8
from sympy import cos, sin, exp
from sympy import Matrix, I
from sympy import pprint
from tqdm import tqdm
from time import time as tm
from sympy.physics.quantum import Dagger

# Each single-qubit gate is parametrised as qiskit's U gate, without the factor 1/2 for theta because we're nice people
def U(th,phi,la, gate='U'):
    mat = sy.MutableDenseNDimArray(range(2*2), (2,2), commutative=False)

    # U-gate
    mat[0,0] = cos(th)
    # two possible choices: expand the exp or not (sympy won't do it by itself)
    # 1) don't expand
    mat[0,1] = -exp(1j*la) * sin(th) 
    mat[1,0] = exp(1j*phi) * sin(th)
    mat[1,1] = exp(1j*(phi+la)) * cos(th)
    # 2) expand
    mat[0,1] = -sin(th) * (cos(la) + 1j * sin(la)) #-exp(1j*la) * sin(th) <- sympy won't expand the exp by itself 
    mat[1,0] = sin(th) * (cos(phi) + 1j * sin(phi)) #exp(1j*phi) * sin(th)
    mat[1,1] = cos(th) * (cos(phi+la) + 1j * sin(phi+la))#exp(1j*(phi+la)) * cos(th)

    if gate=='RX':
        mat[0,0] = cos(th) 
        mat[0,1] = -1j * sin(th) 
        mat[1,0] = -1j * sin(th) 
        mat[1,1] = cos(th) 
    if gate=='RY':
        mat[0,0] = cos(th)
        mat[0,1] = -sin(th)
        mat[1,0] = +sin(th)
        mat[1,1] = cos(th)
    if gate=='RZ':
        mat[0,0] = cos(la) - 1j * sin(la)#exp(-1j * la)
        mat[0,1] = 0
        mat[1,1] = cos(la) + 1j * sin(la)#exp(+1j * la)
        mat[1,0] = 0
    return sy.Matrix(mat)

ng = 4 #number of gates
seq = ['RY' if i%2==0 else 'RX' for i in range(ng)]
seq = seq[::-1]
#seq = ['RY', 'RX'][::-1] # reverse to write in a (hopefully) natural order


TH  = sy.symbols('th0:%d' %ng,  real=True) #telling sympy these are real is useful for conjugation
PHI = sy.symbols('phi0:%d' %ng, real=True)
LA  = sy.symbols('la0:%d' %ng,  real=True)

vec  = sy.Matrix([1,1]) #don't normalise bc it looks ugly 
Gate = sy.Identity(2)

for i in tqdm(range(ng)): #this is fast and O(ng)
    vec  = U(TH[i], PHI[i], LA[i], gate=seq[i]) * vec
    Gate = U(TH[i], PHI[i], LA[i], gate=seq[i]) * Gate

lin_vec = sy.Matrix([1,1]) #will hold the linearised trig functions
lin_Gate = sy.Matrix(sy.Identity(2))
nb_terms, nb_terms_Gate= [], []

for i in tqdm(range(2)): #this is O(exp^ng)
    lin_vec[i] = TR8(vec[i])
    #sy.expand() expands the fractions
    #sy.Add.make_args counts the number of terms in the expanded function
    nb_terms.append(len(sy.Add.make_args(sy.expand(lin_vec[i])))) 

#TODO: do the simplifications at each gate application rather than at the end
for i in tqdm(range(2)):
    for j in tqdm(range(2)):
        lin_Gate[i,j] = TR8(Gate[i,j])
        nb_terms_Gate.append(len(sy.Add.make_args(lin_Gate[i,j])))

print(nb_terms_Gate)

#print(Gate[0,0])
#print(sy.conjugate(TR8(Gate[0,0])))
#print(TR8(Gate[0,0]))
#total = sy.conjugate(sy.transpose(lin_Gate)) * lin_Gate #to check unitarity
#print(total)
w = sy.expand(lin_Gate[0,:]) #action on |0>
print()
#print((sy.expand(TR8(Gate[0,1]))))

expval_Z = Dagger(Gate) * sy.Matrix([[1.0, 0.0], [0.0, -1.0]]) * Gate

# Applying the circuit to |0> selects the [0,0] component 
# TODO: it might be possible to inspect expval_Z to find which entry is richest
Z0  = expval_Z[0,0]
#Z0s = sy.simplify(Z0)
print('Starting simplification')
s = tm()
Z0s = sy.simplify(Z0)
print('Simplification done in ', tm() - s, ' s')

print(Z0s)
ops = Z0s.count_ops(visual=True) # count the number of operations, which are here sines, cosines, sums, prods
print(ops)


