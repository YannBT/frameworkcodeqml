'''
Lists of parameters for quantum and classical networks.
The available values are:

    Shared parameters
    - Types: 'mlp' (classical multi-layer perceptron) and 'qnn' (quantum neural network)
    - DataNames: any .h5 file present in the same directory as main.py
    - Depths: any strictly positive integer
    - Passes: any strictly positive integer
    - Opts: all optimisers of PyTroch (see https://pytorch.org/docs/stable/optim.html). See class_hyper.py for more info.
    - Costs: several loss functions of PyTorch (see https://pytorch.org/docs/stable/nn.functional.html). See class_hyper.py for more info.
    - TrainProps: any number in ]0;1[
    - BatchSizes: any stricly positive integer
    - epochs: any strictly positive integer

    Classical parameters
    - Shapes: any of funnel, long_funnel, brick, triangle, rhombus, diamond, hexagon, hourglass, long_hourglass. See convenience_mlp.py for more info
    - Activations: any of tanh, relu, sigmoid, elu, leakyrelu. See convenience_mlp.py for more info
    - MaxNeurons: any strictly positive integer

    Quantum parameters
    - Circuits: RUC only. TTN2 is currently broken
    - Seqs: any list of one-qubit gates present in convenience_qnn.py: I, X, Y, Z, RX, RY, RZ, Rot
    - Qubits: [1] only (TTN2 currently broken)
'''

# Shared parameters
Types = ['qnn']
DataNames = ['sin1']
Depths    = [1]
Passes   = 1 # always pointless to have a list
Opts     = ["Adam"]
Costs    = ["MSE"] #TODO: custom cost funcs
TrainProps = [0.75] # 0.5 = random pick, works fine
BatchSizes = [1]   # 1 = random pick, works fine
epochs   = 100 # always pointless to have a list

# Classical NN parameters
Shapes = ['brick'] # somewhat equiv to Circuits
Activations = ['Tanh'] # somewhat equiv to Seqs #NOTE: reLU and the like cannot go negative
MaxNeurons = [1] #somewhat equiv to Qubits, keep it low during tests to avoid overfitting

# Quantum NN parameters
Circuits = ['TTN2']#, 'TTN2'] # somewhat equivalent to Shapes
Seqs = [['CRX', 'CNOT']] # somewhat equiv to Activations
#Seqs = [['RX', 'RZ']] # somewhat equiv to Activations
Qubits   = [2] # somewhat equiv to MaxNeurons


# Create shared, classical and quantum dictionnaries
SharedDic     = {'DataNames': DataNames, 'Epochs': [epochs], 'Depths': Depths, 'Opts': Opts, 'Costs': Costs, 
        'TrainProps': TrainProps, 'BatchSizes': BatchSizes}
ClassicalDic  = {'Shapes': Shapes, 'Activations': Activations, 'MaxNeurons': MaxNeurons, 'Types': ['Classical']} #etc
QuantumDic    = {'Circuits': Circuits, 'Qubits': Qubits, 'Seqs': Seqs,'Types': ['Quantum']}
# Quick debugging checks
assert(k[-1]=='s' for k in SharedDic)
assert(k[-1]=='s' for k in ClassicalDic)
assert(k[-1]=='s' for k in QuantumDic)

