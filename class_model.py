import circuits
from class_data import Data
from convenience_qnn import nbrParamGates
import torch
from pennylane import numpy as np
import circuits

# Basis class for one-qubit re-uploaders
#TODO: inherit from torch.nn.Module?
class Model():#torch.nn.Module):

    def __init__(self, circuit_name, block, nb_qubits, depth, loss, data, downscaling=2): #TODO: fix downscaling
        #super(Model, self).__init__() # inherit from torch.nn.Module
        self.type = 'qnn'

        # Circuit-related parameters
        self.name = circuit_name
        self.circuit   = circuits.get_circuit(circuit_name)
        self.block = block
        self.nb_qubits = nb_qubits
        self.depth = depth #FIXME?
        self.downscaling = downscaling if self.name in ['TTN2', 'TTN', 'MERA'] else None #FIXME this looks quite ugly
        self.nb_gates  = self.nb_gates()
        self.seq       = self.gen_seq() #e.g. ['RY', 'RX']

        # Data-related parameters
        self.data = data
        self.nb_params = self.nb_gates * (data.dim_input + 1) #FIXME: valid only if all rotations upload the data

        # Misc
        self.list_nb_params = self.calc_list_nb_params() # nbr of params for the gates
        self.list_nb_angles = self.calc_list_nb_angles()
        self.nb_angles = np.sum(self.list_nb_angles)

        # Parameters initialisation and rescaling
        self.params = torch.rand(self.nb_params, requires_grad=True)
        self.params = self.params.add(-.5).clone().detach().requires_grad_(True) #preferred syntax, cf the docs
        self.params = self.params.multiply(2*torch.pi).clone().detach().requires_grad_(True) 
        
        # Optimisation related parameters
        self.lossName = loss
        self.loss = 0 # value of the loss
        self.TrainLosses = []
        self.TestLosses = []
        self.Params = []
    
    # Set the path to the location of this model's outputs
    def set_path(self, path):
        self.path = path

    # forward func mimicking the MLP one
    def forward(self, train_input):
        preds = torch.empty((len(train_input))) #self.data.dim_output)) #FIXME:nbr of cols depending on shape of output?
        i = 0 #index
        for inp in train_input:
            angles = self.calcAngles(inp)
            val_pred = self.circuit(angles, self)
            preds[i] = val_pred#.double()
            i += 1
        return preds

    # Function computing the angles for a given data point
    def calcAngles(self, x):
        dim = self.data.dim_input # equiv to len(x)
        angles = []
        for i in range(self.nb_gates): #nb_gates
            w = self.params[i*dim:(i+1)*dim]
            b = self.params[(i+1)*dim]
            angle = torch.dot(w, x) + b
            angles.append(angle)
        return angles
    
    # Functions to assign gate <- nb angles <- nb params
    # TODO: replace by PennyLane rotations attributes
    def calc_nb_angles(self, gate_name):
        if gate_name in nbrParamGates.keys(): return nbrParamGates[gate_name]
        else: raise ValueError('Gate name %s not recognised' %s)

    def calc_list_nb_angles(self):
        return [self.calc_nb_angles(gate) for gate in self.seq]
        
    def calc_list_nb_params(self): #TODO: this should be able to consider the reuploading scheme, e.g. R(wx+b)R(phi)
        return [(self.data.dim_input + 1) * self.calc_nb_angles(gate) for gate in self.seq]
    
    def gen_seq(self):
        seq = []
        for n in range(self.nb_gates):
            seq += self.block
        return seq[:self.nb_gates] #creates paths containing spaces in save.py

    def nb_gates(self):
        if self.name == 'RUC': return self.depth
        elif self.name == 'TTN2': return (self.downscaling)**self.depth - 1
