import numpy as np
import h5py
import challenges

n = 30

x = (np.random.rand(n) - 0.5) * 2 * np.pi
y = (np.random.rand(n) - 0.5) * 2 * np.pi
f = np.sin(x+y) #must be exactly learnable by a single rotation

newX = [[vx, vy] for vx, vy in zip(x,y)]
newF = [[v] for v in f]

challenges.save_challenge(newX, newF, 'sin2d_mm')


