#!/usr/bin/env python3
# coding: utf-8

import sys
from challenges import *
import numpy as np
import torch
from torch.utils import data
import matplotlib.pyplot as plt

print(sys.argv)
if len(sys.argv)<2:
        print("usage : %s challenge_name"%(sys.argv[0]))
        sys.exit(1)
name=sys.argv[1]

x,y,train_loader,test_loader=load_challenge(name)
plot_challenge(x,y,name)
model=torch.load("%s.pth"%(name))
plot_classif(x,y,model)
grid_search(x,y,model)

