import numpy as np
import matplotlib.pyplot as plt

def gen(challenge_ID, nb_pts):
    pos, col = [], []

    if challenge_ID == 1: #two Gaussians
        #this one is 99% from Frederic's tuto
        #the only mods are the rescaling of the data and of the FWHM
        s = 1
        for i in range(1000):
            # first cluster
            t = np.random.multivariate_normal([0,0], [[0.4/s,0.], [0.,0.4/s]])

            pos.append(t)
            col.append(0)

            # second cluster
            t = np.random.multivariate_normal([3.5/s,3.5/s], [[0.6/s,0.4/s], [0.4/s,0.6/s]])
            pos.append(t)
            col.append(1)

        #force the data into [0,1]x[0,1]
        pos -= np.min(pos)
        pos /= np.max(pos)
        return pos, col

    x, y = np.random.rand(nb_pts), np.random.rand(nb_pts)
    f = 0

    if challenge_ID == 4: #concentric circles
        x0, y0 = 0.5, 0.5
        r = np.sqrt((x-x0)**2 + (y-y0)**2)
        for i in range(nb_pts):
            pos.append([x[i],y[i]])
            if np.floor(9*r[i]) % 2 == 0: #the factor controls the nbr of circles
                col.append(0)
            else : 
                col.append(1)


    if challenge_ID == 5: #three somewhat Gaussians with one shallow basin
        f = np.exp(-(x-2)**2) + np.exp(-(x-6)**2 /10) + 1./(x**2 +1)
        # taylor f a bit to force it into [0,1]x[0,1]
        f /= np.max(f) 
        # affect the data to either cluster
        for i in range(nb_pts):
            pos.append([x[i],y[i]])
            if y[i] > f[i] : col.append(0)
            else : col.append(1)

    return pos, col





