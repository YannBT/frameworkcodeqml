#import convenience_qnn
#import convenience_mlp
from convenience_qnn import isConsistent as check_qnn
from convenience_mlp import isConsistent as check_mlp

# Flatten a list of lists. Used for flattening sequences,
# e.g. ['RX','RY'] -> unchanged, [['RX', 'RY'], 'CNOT'] -> ['RX','RY','CNOT']
# Currently used to determine the number of rotations of a circuit,
# and for ugly reshaping tricks in the plots.py module
def flatten(l,flat):
    for sub in l:
        if isinstance(sub, list):
            flatten(sub,flat)
        else:
            flat.append(sub)
    return flat

def isConsistent(dic):
    # Classical networks are always constructed consistent
    if dic['type'] == 'Classical': return check_mlp(dic)
    if dic['type'] == 'Quantum': return check_qnn(dic)
    else:
        raise KeyError('Network corresponding to %s not recognised'%dic)

# Almighty functions regenerating a model in its stored state from a dic
#FIXME doing only QNN for now (MLPs break json)
def regenModel(path):
    return 0# NEEDED OBJECTS: 
    # - losses
    # - params
    # - train & test data
    # - ?
    #from glob import glob
    #pth = [f for f in glob(path + '*.pth')]
    #if not (len(pth) == 1):
    #    print('Expected 1 .pth file in %s but found %s' %(path,len(pth)))
    #pth = pth[0]
    ##print(pth)
   
    #data_dic = [f for f in glob(path + 'data.dic')]
    #if not (len(data_dic) == 1):
    #    print('Expected 1 data.dic file in %s but found %s' %(path,len(data_dic)))
    #data_dic = data_dic[0]



    #from class_mlp import Mlp
    #model = Mlp(*args, **kwargs)
    #model.load_state_dict(torch.load(pth))
    #model.eval()
    #if dic['type'] == 'Classical':
    #    print('regen mlp instance')
    #from class_model import Model
    #model = Model(dic['circuit'], dic['seq'], dic['qubit'], dic['depth'], dic['cost'], data) 






