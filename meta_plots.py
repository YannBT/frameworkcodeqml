import Global
import random
import os
import sys
from glob import glob
import matplotlib.pyplot as plt
import plt_params as params
import pandas as pd
from itertools import product
import numpy as np
from scipy.optimize import curve_fit
from time import time as tm
import seaborn as sns
#sns.set_theme(style='darkgrid')

date = Global.TODAY
#date = '18-05-2022'
TODAY = date
sep = Global.SEP
trim = Global.TRIM
nbr_std = Global.NBR_STD

colours=['tab:red', 'tab:brown', 'tab:green', 'tab:pink', 'tab:grey'] * 100 #FIXME: this because plt_params cycles
colours=['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:grey'] * 100 #FIXME: this because plt_params cycles
ls = ['-', '--', ':', '-.'] * 100 #patch
mk = ['o', 's', 'X', 'D', 'p'] * 100
al = lambda j: 0.5/j
mke=0.1
def f(x,a,b,c): #to fit loss functions
    return a * np.exp(-b*x) + c

# TODO: user options:
# - date of the data
# - etc
# -
# -

# Go down all folders inside PATH, group all .csv files with identical names together
def list_all_paths(PATH='/outputs-' + TODAY + '/'):
    PATH = Global.ROOT + PATH
    #for (root, dirs, files) in os.walk(PATH):
    #    print()
    #    print(root)
    #    print(dirs)
    #    print(files)
    f = open(PATH + 'all_paths.txt', 'r')
    all_paths = [path[:-1] for path in f]

    #probe a few paths to get an idea of the total size of the files
    k = np.min([len(all_paths), 1000])
    sample = random.sample(all_paths, k)
    sizes  = []
    for s in sample:
        size = sum([os.path.getsize(f) for f in glob(s + '*')])
        sizes.append(size)    
    
    mean = int(np.ceil(np.mean(sizes)))
    std  = int(np.ceil(np.std(sizes)))
    tot  = mean*len(all_paths)
    
    # format the size into something legible
    unit = 1000
    p = int(np.floor(np.log(tot) / np.log(unit)))
    prefixes = ['', 'K', 'M', 'G', 'T', 'P']
    if p > 4: 
        print('Asking to read %s bytes, cancelling' %tot)
        sys.exit()
    tot = np.round(tot / unit**p, 3)
    std = np.round(std / unit**p, 3)
    p = prefixes[p]

    if k < len(all_paths):
        print('Sampled %s/%s paths, expecting to load %s %sB, +/- %s %sB.' %(k,len(all_paths), tot, p, np.round(3*std,3), p))
    else:
        print('Sampled %s/%s paths, expecting to load %s %sB.' %(k,len(all_paths), tot, p))

    return all_paths
    
# select all paths according to some criteria 'crits', avoiding those where any in 'avoid' is found
def select_paths(paths, picks=None, avoid=None):
    assert(type(picks) == list or picks == None), 'given type for picks: %s'%type(picks)
    assert(type(avoid) == list or avoid == None), 'given type for avoid: %s'%type(avoid)
    
    selected = paths
    if picks != None:
        selected = [p for pick in picks for p in paths if pick in p] 
    if avoid != None:
        selected = [p for av in avoid for p in paths if av not in p]

    return selected


# rebuild a dic from slicing its path
def rebuild_dics(paths): #TODO: fetch the keys where appropriate, e.g. from Global
    if type(paths) == str:
        raise TypeError('Paths should be a list of paths, not a single path')
    assert(type(paths) == list)

    list_dics = []
    for path in paths:
        split_path = path.split('/')
        dic = {}
        for i in split_path:
            if i.find(sep) > 0:
                j = i.split(sep)
                key, value = j[0], j[1]

                ''' TODO: this when KEYS are properly saved in Global. CAVEAT: this code does not work anymore if KEYS change
                # workaround: add bool STRICT_KEYS_CHECK in Global?
                if key not in KEYS:
                    raise KeyError('Key \'%\' not found among the available ones (%s)' %(key,KEYS))
                '''

                dic[key] = value
        list_dics.append(dic)
    return list_dics

def rebuild_paths(dics, PATH='/outputs-' + TODAY + '/'): #input: list of dics
    assert(type(dics) == list)
    assert(type(d) == dict for d in dics)
    PATH = Global.ROOT + PATH

    list_paths = []
    for d in dics:
        path = PATH
        for k, v in d.items():
            path += k + '_' + v + '/'
        list_paths.append(path)
    return list_paths

# group paths that differ only in their values of keys
# FIXME: the name of this func is counterintuitive wrt what it does
def group_batches(paths, keys):
    assert(type(keys)==list)

    # Rebuild the dics corresponding to the paths for clean popping
    dics = rebuild_dics(paths)
    for key in keys:
        try:
            [d.pop(key) for d in dics]
        except:
            raise KeyError('Could not pop %s key from the dics. Did you mess up with save.py?' %key)

    # Build the paths to the truncated dics
    cut_paths = rebuild_paths(dics)
    # Remove multiple occurences
    cut_paths = list(set([p for p in cut_paths]))

    return cut_paths, keys

# average all output files accross a batch
# FIXME: for now, searching losses_df.csv only
def average_batches(paths):
    start = tm()
    paths_without_run = group_batches(paths, ['run'])[0]
    print(paths_without_run)
    # Iterate over all different batches
    # Within each batch, retrieve the runs it contains
    for pwr in paths_without_run:
        runs = []
        for path in paths:
            if pwr in path: 
                runs.append(path)

        for csvFile in glob(runs[0] + '*csv'):
            csvFile = csvFile.split(runs[0])[1] #keep only the filename, not the full path
           
            # patch here
            if 'params' in csvFile: 
                #print(csvFile, ' cannot be hashed because it contains torch tensors')
                continue
            # other path: read only losses for now
            if 'loss' not in csvFile:
                #print(csvFile, ' cannot be hashed because it contains torch tensors')
                continue

            dfs = [] # stack csvFile for all runs

            # Not all df have the same length due to early stopping. Get max length
            # to fill in gaps with zeros, to avoid wrong results when taking avgs & stds
            maxLength = 0
            for path_to_run in runs:
                df = pd.read_csv(path_to_run + csvFile)
                if len(df) > maxLength: maxLength = len(df)
            for path_to_run in runs:
                #TODO: loop over all .csv
                df = pd.read_csv(path_to_run + csvFile)
                while len(df) < maxLength: #append rows
                    df.loc[len(df.index)] = [0 for _ in range(len(df.columns))]
                dfs.append(df)
            
            # not all df have the same length due to early stopping: fill in the gap
            # to avoid wrong results when taking averages & stds
            
            dfs = pd.concat(dfs, axis=0) #place the df next to each other (axis=1) or on top (axis=0)
            
            # Average the stack of dataframes
            # NOTE: dfs do not have the same length, one can (i) append missing rows, (ii) avoid using .mean() etc
            # I append rows and check that the results are identical with both methods
            df_avg = dfs.groupby(level=0).mean()
            #print(len(glob(runs[0] + '*csv')))
            #df_avg_byhand = dfs.groupby(level=0).sum() / 100#len(glob(runs[0] + '*csv'))
            #print(df_avg_byhand == df_avg)
            #assert(df_avg_byhand.equals(df_avg))
            df_std = dfs.groupby(level=0).std() # same
          

            #TODO: to not double count the index columns
            for key in list(set(dfs.columns)):
                avg = df_avg[key]
                std = df_std[key]
            
            # Store the averaged results
            df_avg.to_csv(pwr + csvFile[:-4] + '_avg.csv')
            df_std.to_csv(pwr + csvFile[:-4] + '_std.csv')
            # Store the stacked dataframes. Safety net, unlikely to be used
            dfs.to_csv(pwr + csvFile[:-4] + '.csv')
    t = tm() - start
    print('Averaged all batches in %.3f secs.' %t)

def plot_batches(paths_without_run):
    assert(type(paths_without_run) == list)
    # FIXME: for now, searching losses_df.csv only
    # TODO: plot all possible pairs of quantities?
    for path in paths_without_run:
        fig, ax = plt.subplots()

        csvFiles = glob(path + '*.csv')
        csvFiles = [f.split(path)[1] for f in csvFiles]
       
        # Group tuples of (std, avg)
        avgsFiles = [f for f in csvFiles if 'avg' in f]
        stdsFiles = [f for f in csvFiles if 'std' in f]
        
        #FIXME: assumes avgs and stds pairwise match, which is not guaranteed
        for avgFile, stdFile in zip(avgsFiles, stdsFiles):
            avg = pd.read_csv(path + avgFile)
            std = pd.read_csv(path + stdFile)

            cols = None
            if 'loss' in avgFile:
                cols = ['Train loss', 'Test loss']
            else:
                raise KeyError('Unable to understand what %s contains' %avgFile)
            
            idx = 0 #idx col of dataframe
            for col in cols:
                epochs = range(1,1+len(avg[col])) #to have epochs starting at 1
                epochs = range(len(avg[col])) #to have epochs starting at 0
                ax.plot(epochs, avg[col], label=col, color=colours[idx], ls=ls[idx], markevery=mke)

                for i in range(1,nbr_std+1):
                    ax.fill_between(epochs, avg[col]-i*std[col], avg[col]+i*std[col], alpha=al(i), color=colours[idx], ls=ls[idx])
                idx += 1
        
        plt.xlabel('Epochs')
        plt.ylabel('Losses')
        plt.legend(loc='best')
        plt.show()
        plt.clf()

#old version, groups according to each key individually
def oldgroupby(paths, keys):
    '''
    Input: list of paths, list of keys
    Output: list of lists, grouped by values of keys
    '''
    dics = rebuild_dics(paths)

    splitlist = []
    for key in keys:
        possible_vals = list(set(d[key] for d in dics))
        for val in possible_vals:
           l = [d for d in dics if d[key] == val]
           splitlist.append(rebuild_paths(l))
    return splitlist 

def groupby(paths, keys):
    '''
    Input: list of paths, list of keys
    Output: list of lists, grouped by values of keys
    '''
    dics = rebuild_dics(paths)

    possible_vals = {}
    pv = []
    for key in keys:
        possible_vals[key] = list(set(d[key] for d in dics))
        pv.append(list(set(d[key] for d in dics)))

    splitlist = []
    # Iterate over all combinations of values, group accordingly
    for v in product(*pv):
        l = []
        for d in dics:
            if all(d[key] == val for key, val in zip(keys,v)):
                l.append(d)
        splitlist.append(rebuild_paths(l))
    '''
    splitlist = []
    for key in keys:
        possible_vals = list(set(d[key] for d in dics))
        for val in possible_vals:
           l = [d for d in dics if d[key] == val]
           splitlist.append(rebuild_paths(l))
    '''
    return splitlist

def meta_plot(abscissa, ordinate, label, paths_without_runs):
    
    if abscissa not in Global.ALLOWED_ABS: raise KeyError('Key %s for x-axis not recognised. Use any of %s' %(abscissa, Global.ALLOWED_ABS))
    if ordinate not in Global.ALLOWED_ORD: raise KeyError('Key %s for y-axis not recognised. Use any of %s' %(ordinate, Global.ALLOWED_ORD))

    # List of lists according to the label key
    split_by_label = groupby(paths_without_runs, label)

    # FIXME: for now, searching losses_df.csv only
    fig, ax = plt.subplots()

    idx=0
    for sublist in split_by_label:
        dataAvg, dataStd, absVals = [], [], []
        for path in sublist:
            _abs, avg, std, lab = extract(abscissa, ordinate, label, path)
            #FIXME: is a switch for 'scalar' (e.g. nbParam) vs 'vector' (e.g. epoch) worth it?
            if abscissa == 'epoch':
                ax.plot(_abs, avg, marker=None, color=colours[idx], label=lab, markevery=mke)
                for j in range(1, nbr_std+1):
                    ax.fill_between(_abs, avg-j*std, avg+j*std, color=colours[idx], alpha=al(j))          
                # try fitting with an exponential
                popt, pcov = curve_fit(f, _abs, avg, bounds=(0, 5))
                #ax.plot(_abs, f(_abs, *popt), color=colours[idx])
            else:
                absVals.append(float(_abs))
                dataAvg.append(float(avg))
                dataStd.append(float(std))
        # reorder data if needed
        if abscissa != 'epoch':
            order = np.asarray(absVals).argsort()[::1]
            absVals = np.asarray(absVals)[order]
            dataAvg = np.asarray(dataAvg)[order]
            dataStd = np.asarray(dataStd)[order]
            ax.fill_between(absVals, dataAvg-dataStd, dataAvg+dataStd, color=colours[idx], alpha=al(1))
            ax.plot(absVals, dataAvg, marker=mk[idx], color=colours[idx], label=lab)
        idx += 1
    ax.set_xlabel(abscissa)
    ax.set_ylabel(ordinate)
    plt.legend()
    plt.tight_layout()
    plt.savefig('some_meta_plot.pdf')
    plt.show()
        
        
# extract the desired data from a single path + format the corresponding label
def extract(_abs, _ord, _label, path):
    dic = rebuild_dics([path])[0]
    
    x_val = None
    y_avg = None
    y_std = None

    what = 'losses_df' if 'loss' in _ord else None

    avg = pd.read_csv(path + what + '_avg.csv')
    std = pd.read_csv(path + what + '_std.csv')

    # extract abscissa and ordinate data
    if _ord in Global.ALLOWED_ORD:
        # inevitable abscissa-dependent fine-tuning
        if _abs == 'epoch':
            y_avg = avg[_ord]
            y_std = std[_ord]
        if _abs == 'nbParams':
            y_avg = avg[_ord].tail(1).values.item()
            y_std = std[_ord].tail(1).values.item()

    if _abs in dic.keys():
        value = dic[_abs]
        # inevitable abscissa-dependent fine-tuning
        if _abs == 'epoch':
            x_val = range(int(len(y_avg)))
        if _abs == 'nbParams':
            x_val = value
    
    # Format the label(s)
    label = ''
    for l in _label:
        if l not in dic.keys():
            raise KeyError('Label %s not recognised, should be one of %s' %(_label, dic.keys()))
        else: label += dic[l] + ' '
    label = label[:-1]
    
    return x_val, y_avg, y_std, label

all_paths = list_all_paths()
print(all_paths)
average_batches(all_paths) #NOTE: always call this on all_paths
all_paths_without_run = group_batches(all_paths, ['run'])[0] #paths to averaged dataframes

# TODO: use plot_batches again
#plot_batches(all_paths_without_run) #TODO: labels etc

#one_path = select_paths(all_paths, ['003'])
#print(groupby(all_paths_without_run, ['type', 'dataName']))

#meta_plot('epoch', 'Train loss', ['nbParams', 'dataName','type'], all_paths_without_run)

some_paths = select_paths(all_paths_without_run, picks=['sin1', 'TTN2'], avoid=['gauss1'])
meta_plot('nbParams', 'Train loss', ['dataName', 'type', 'circuit', 'seq'], some_paths)
