import torch

def lp(params, f, circuit, x, p=2):
    return torch.abs( (circuit(params, x) - f(x))**p )

def l2(params, f, circuit, x):
    return torch.abs( (circuit(params, x) - f(x))**2 )

def l4(params, f, circuit, x):
    return torch.abs( (circuit(params, x) - f(x))**4 )
