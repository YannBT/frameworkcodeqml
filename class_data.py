import sys
import Global # contains paths
#sys.path.insert(0, Global.PATH_CHALLENGES)
from challenges import load_challenge

# Class that contains the datasets and all related relevant information
# Currently, serves as a wrapper of the data and related information# Currently, serves as a wrapper of the data and related information

class Data:

    def __init__(self, name_data, train_prop=0.50, batch_size=1):
        # Frederic Magniette's data loading function
        self.data_inputs, self.data_outputs, self.train_loader, self.test_loader = load_challenge(name_data, train_prop=train_prop, batch_size=batch_size)

        # Relevant parameters of the data
        # TODO: am I missing some?
        self.name = name_data
        self.nb_data     = len(self.data_inputs)
        self.nb_traindata = int(train_prop * self.nb_data)
        self.nb_testdata = self.nb_data - self.nb_traindata
        #self.batch_size  = next(iter(self.train_loader))[0].size()[0]
        self.batch_size = batch_size
        self.dim_input   = len(self.data_inputs[0]) 
        self.dim_output   = 1 #len(self.data_outputs) #FIXME: Fred does not store each output as a (1d) list
        
        # Flatten data into lists. Currently used only in save.py.
        train_inputs  = []
        train_outputs = []
        for in_train, out_train in self.train_loader:
            for elemIn, elemOut in zip(in_train, out_train):
                train_inputs.append(elemIn)
                train_outputs.append(elemOut)

        test_inputs  = []
        test_outputs = []
        for in_test, out_test in self.test_loader:
            for elemIn, elemOut in zip(in_test, out_test):
                test_inputs.append(elemIn)
                test_outputs.append(elemOut)
        self.train_inputs   = train_inputs
        self.train_outputs  = train_outputs 
        self.test_inputs    = test_inputs 
        self.test_outputs   = test_outputs 

        #self.dic = self.gen_dic()
    # Generate a dict containing all of the class' attributes, for easy saving
    #FIXME: this looks deprecated, but I'm checking this
    #def gen_dic(self):
    #    #attributes = dir(self)
    #    #attributes = [a for a in attributes if '__' not in a]
    #    #print(attributes)
    #    #a = attributes[0]
    #    #print(a)
    #    #print(self.a)
    #    
    #    
    #    dic = {'Name': self.name,
    #            'Nb data (total)': self.nb_data,
    #            'Nb data (train)': self.nb_traindata,
    #            'Nb data (test)': self.nb_testdata,
    #            'Batch size': self.batch_size,
    #            'Dimension (input)': self.dim_input,
    #            'Dimension (output)': self.dim_output,
    #            'Data (input)': self.data_inputs,
    #            'Data (output)': self.data_outputs,
    #            'Train data (input)' : self.train_inputs,
    #            'Train data (output)' : self.train_outputs,
    #            'Test data (input)' : self.test_inputs,
    #            'Test data (output)' : self.test_outputs
    #            #'Loader (train)': self.train_loader,
    #            #'Loader (test)': self.test_loader,
    #            }

    #    return dic


