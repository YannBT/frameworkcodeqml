import torch
import numpy as np
# Convenience function to generate a list of (neurons, activ)
# The last layer automatically has dim_output neurons
def gen_archi(nb_layers, act, nb_neurons, dim_output, geom):
    act = get_activation(act) #new patch

    Geom = make_scaled_shape(nb_layers, nb_neurons, geom)
    layer_params = [(layer, act) for layer in Geom]
    
    #layer_params.append[(dim_output, torch.nn.Sigmoid())] #NOTE: use an activation that's consistent with data normalisation, e.g. NOT SIGMOID FOR DATA IN [-1;1]
    #layer_params.append(tuple([dim_output, torch.nn.Tanh()]))
    layer_params.append(tuple([dim_output, act]))
    
    return layer_params

# Return a torch.nn activation function from a string
# This because JSON cannot store objects
def get_activation(name):
    name = name.lower()
    if name == 'tanh': return torch.nn.Tanh()
    if name == 'relu': return torch.nn.Relu()
    if name == 'sigmoid': return torch.nn.Sigmoid()
    if name == 'elu': return torch.nn.ELU()
    if name == 'leakyrelu': return torch.nn.LeakyReLU()


# Convenience geometry-generating functions
# Create a list of neurons numbers corresponding to a given shape. neuron_min is used only for auto-encoding-type and diamond shapes
def make_shape(nb_layers, neuron_max, neuron_min=1, shape='funnel'):
    divide = lambda n: int(np.ceil(n*0.5)) #shorthand
    # TODO: switch for pointy triangles (n_out=1) or not
    layers = []
    if shape == 'funnel':
        layers.append(neuron_max)
        [layers.append( divide(layers[k-1]) ) for k in range(1, nb_layers)]

    if shape == 'long_funnel':
        half = int(np.ceil(nb_layers * 0.5))
        layers = [neuron_max for k in range(half)]
        [layers.append( divide(layers[k-1]) ) for k in range(half, nb_layers)]

    if shape == 'brick':
        [layers.append(neuron_max) for k in range(nb_layers)]

    if shape == 'triangle':
        layers = make_shape(nb_layers, neuron_max, shape='funnel')[::-1] #mastermind move

    if shape == 'rhombus':
        c = (neuron_max - 1) / (np.ceil(nb_layers/2) - 1)
        top = [int(np.ceil(c*i + 1)) for i in range(int(nb_layers/2))]
        bot = top[::-1]
        if nb_layers % 2:
            top.append(neuron_max)
        layers = top + bot

    if shape == 'diamond': #different from Talos'
        waist = neuron_min
        top = make_shape(waist+1, neuron_max, shape='triangle')[:-1]
        bot = make_shape(nb_layers-waist, neuron_max, shape='funnel')
        layers = top + bot

    if shape == 'hexagon': #different from Talos'
        top = make_shape(int(np.ceil(nb_layers/3)), neuron_max, shape='funnel')[::-1]
        bot = make_shape(int(np.ceil(nb_layers/3)), neuron_max, shape='funnel')
        while len(top+bot)<nb_layers:
            top.append(neuron_max)
        layers = top + bot

    # Auto-encoding-suited geometries
    if shape == 'hourglass':
        top = make_shape(int(nb_layers/2), neuron_max, shape='funnel')
        bot = make_shape(int(nb_layers/2), neuron_max, shape='triangle')
        if nb_layers % 2:
            top.append(neuron_min)
        else:
            top[-1], bot[0] = neuron_min, neuron_min
        layers = top + bot

    if shape == 'long_hourglass':
        top = make_shape(int(nb_layers/2), neuron_max, shape='long_funnel')
        bot = make_shape(int(nb_layers/2), neuron_max, shape='long_funnel')[::-1]
        if nb_layers % 2:
            top.append(neuron_min)
        else:
            top[-1], bot[0] = neuron_min, neuron_min

        layers = top + bot

    # final clamping of the neuron numbers
    l      = [e if e>=neuron_min else neuron_min for e in layers]
    layers = [e if e<=neuron_max else neuron_max for e in l]

    assert(len(layers) == nb_layers)
    assert(np.max(layers) == neuron_max)
    return layers

# Construct a geometry with number of neurons closest to target, fixed nb_layers
def make_scaled_shape(nb_layers, nb_neurons, geom):
    Min, Max = 1, nb_neurons
    TotMax = np.sum(make_shape(nb_layers, nb_neurons, shape=geom))
    TotMin = np.sum(make_shape(nb_layers, 1, shape=geom))

    Nbr = nb_neurons
    a,i = nb_neurons, 0
    while i<a:
        Nbr = int((TotMax+TotMin)/2)
        Tot = np.sum(make_shape(nb_layers, Nbr, shape=geom))
        if Tot > nb_neurons: TotMax = Nbr
        else: TotMin = Nbr
        i+=1
        #print(make_shape(nb_layers, Nbr, shape=geom))
    return make_shape(nb_layers, Nbr, shape=geom)

def isConsistent(dic):
    # Some loss functions take only positive values,
    # while some activations can return negative values
    # Check that the pair (activation, loss) is consistent 

    # TODO: be exhaustive or find a less dumb way of testing
    positiveLosses = ['BCE']
    negativeLosses = ['MSE']
    positiveActiv  = [torch.nn.Sigmoid(), torch.nn.ReLU()]
    negativeActiv  = [torch.nn.Tanh()]

    LP = dic['cost'] in positiveLosses
    LN = dic['cost'] in negativeLosses
    AP = dic['activation'] in positiveActiv
    AN = dic['activation'] in negativeActiv

    #print(dic)
    print(LP, LN, AP, AN)
    #if LP and not(AP): return False
    #if LN and not(AN): return False
    return True
