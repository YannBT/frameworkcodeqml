import numpy as np
import matplotlib.pyplot as plt
from Global import PATH_OUTPUTS
from analytical import calc_expr, calc_angles
import torch
from time import time as tm
import os
import pandas as pd
from save import create_tag
from itertools import product
from glob import glob
from class_model import Model
from class_mlp import Mlp
import plt_params
import challenges # to load exact grid data
from convenience_functions import flatten

from tqdm import tqdm
from copy import deepcopy
# Grocery list
# - circ from tag
# - anal expr from seq + checks circ <=> seq
# - generate grids
# - plot losses
# - plot preds for first epoch, last, best, + some evenly spaced accross epochs
# TODO: do we want something else?

class Grid():
    def __init__(self, model, shape):
        self.dim = len(shape)
        self.X = gen_grid(shape)
        self.nb_pts = shape

        self.nx = self.nb_pts[0]
        self.ny = self.nb_pts[1] if self.dim > 1 else None
        self.ny = self.nb_pts[2] if self.dim > 2 else None

        # FIXME proper normalisation
        fac = 1
        self.xmin = -1 * fac 
        self.xmax = +1 * fac 
        self.ymin = -1 * fac 
        self.ymax = +1 * fac 
        self.zmin = -1 * fac 
        self.zmax = +1 * fac 
        self.boundaries = [-1*fac,1*fac]

        self.xlims = [self.xmin, self.xmax]
        self.ylims = [self.ymin, self.ymax]
        self.zlims = [self.zmin, self.zmax]
        self.extent = (self.xlims + self.ylims + self.zlims)[:self.dim*2]
        
        #TODO: push this into a eval_grid or whatever?
        self.F = eval_model(self, model)
        self.Fmin = np.min(self.F)
        self.Fmax = np.max(self.F)



#FIXME: is 'model' the best arg to pass?
#TODO: should be able to read files AND to plot from a Model
#FIXME: currently only handles Model 
#FIXME: currently calls the calc_angles() func from analytical.py
#TODO: uniformise calc_angles() and calcAngles()
def plot(model, data): #TODO: find a better name?
    #TODO: isinstances()...
    # Within this func, x (and variations thereof) represent inputs,
    # while f (and variations thereof) represents ouputs

    # TODO: save at the right place

    # TODO: move the reading part where it fits
    #path = PATH_OUTPUTS + create_tag(model) + '/' #FIXME: does not consider the tag's number

    df = pd.read_csv(model.path + 'losses_df.csv')
    #plt.plot(df['Epochs'], df['Train loss'])
    #plt.plot(df['Train loss'])
    #plt.show()

    #TODO: switch params vs moments if less than a given nbr of params

    seq  = model.seq if isinstance(model, Model) else None
    params = model.params.detach().numpy() if isinstance(model, Model) else None #should work for both using parameters_to_np_array()
    dim = data.dim_input

    # Figures declaration
    #TODO: adapt to 3d data
    #TODO: different kinds of panels (2x2, 1x4, 4x1...)
    Func, axF = plt.subplots()
    Loss, axL = plt.subplots()
    Diff, axD = plt.subplots()
    Para, axP = plt.subplots()
    Moms, axM = plt.subplots()
    Panel, axs = plt.subplots(nrows=2, ncols=2, figsize=(9,9))#)figsize=(12.8,9.6)) #FIXME: should be soft

    #### Pre-processing and recalculations
    # Train and test data
    try:
        xTr, fTr = unzip(data.train_loader)
        xTe, fTe = unzip(data.test_loader)

        # Grid evaluation
        Xtrue, Ftrue = challenges.load_challenge_exact(data.name)
        shape = len(Xtrue) #FIXME patch
        grid = Grid(model, shape=[shape,shape,shape][:dim])
        X, F = grid.X, grid.F #TODO: should disappear?

        #X, n = gen_grid(data.dim_input) #works with tensor(X) as well
        #s = tm()
        #F = 0 if DEBUGGING else eval_model(model, X, n)
        
        #### Plots

        # train, test, eval
        # TODO add grid plot of target
        axF = plotFunc(axF, xTr, fTr, xTe, fTe, grid)#X, F) #lengthy, but explicit
        Func.tight_layout()
        Func.savefig(model.path + 'fit.pdf')
        Func.clf()
    except: pass

    # losses
    axL = plotLoss(axL, model)
    Loss.tight_layout()
    Loss.savefig(model.path + 'loss.pdf')
    Loss.clf()

    # diffs
    #axD = plotDiff(axD, model, nb_pts, xTr, fTr, xTe, fTe)
    #Diff.tight_layout()
    #Diff.savefig(model.path + 'diff.pdf')
    #Diff.clf()

    #TODO: switch params vs moments according to nbr of params
    # moments of the params w.r.t a given epoch, or wrt 0 if idx=-1
    moments = calc_moments(model, 4, -1)
    axM = plotMoments(axM, moments)
    Moms.tight_layout()
    Moms.savefig(model.path + 'moments.pdf')
    Moms.clf()

    # parameters
    if model.nb_params < 4:
        axP = plotParams(axP, model)
        Para.tight_layout()
        Para.savefig(model.path + 'params.pdf')
        Para.clf()
    
    # panel plot
    axs[0,0] = plotLoss(axs[0,0], model)
    try: axs[0,1] = plotFunc(axs[0,1], xTr, fTr, xTe, fTe, grid)#X, F) 
    except: pass
    if model.nb_params < 4:
        axs[1,0] = plotParams(axs[1,0], model)
    else:
        axs[1,0] = plotMoments(axs[1,0], moments)
    try: axs[1,1] = plotDiff(axs[1,1], data.name, model, grid, xTr, fTr, xTe, fTe)
    except: pass
    from matplotlib.ticker import AutoMinorLocator
    for i in range(2):
        for j in range(2):
            axs[i,j].xaxis.set_minor_locator(AutoMinorLocator(4))
            axs[i,j].yaxis.set_minor_locator(AutoMinorLocator(4))
    Panel.tight_layout()
    #plt.show()
    Panel.savefig(model.path + 'panel.pdf')
    Panel.clf()

    # main purpose: plot a neat gif
    # main feature: heat up processors to 100 degrees
    #plotGif(model, grid) #NOTE: deactivated while eval is too demanding

def plotFunc(ax, xTr, fTr, xTe, fTe, grid, interp='none'):#xGrid, fGrid): #lengthy, but explicit
    dim = len(xTr[0])
    if dim == 1:
        ax.plot(xTr, fTr, ls='', marker='o', color='tab:purple', label='Train', zorder=4) #zorder=4 bc it works
        ax.plot(xTe, fTe, ls='', marker='s', color='tab:orange', label='Test', zorder=4)
        ax.plot(grid.X, grid.F,   ls='-', color='k', label='Fit')
        ax.set_xticks(maketicks(grid.xmin, grid.xmax))
        ax.set_xlabel(r'$x$')
        ax.set_ylabel(r'$\hat{f}(x)$')
        ax.legend(loc='best')
    elif dim == 2:
        #TODO: scatter train and test points
        im = ax.imshow(grid.F, origin='lower', interpolation=interp, extent=grid.extent)
        #boundaries = np.linspace(grid.boundaries[0], grid.boundaries[1], 100) #cbar
        #ticks = np.linspace(grid.boundaries[0], grid.boundaries[1], 5)
        boundaries = np.linspace(grid.Fmin, grid.Fmax, 100) #cbar
        ticks = np.linspace(grid.Fmin, grid.Fmax, 5)
        cbar = plt.colorbar(im, ax=ax, boundaries=boundaries, ticks=ticks)
        #TODO: legend etc
    return ax

def plotLoss(ax, model):
    ax.plot(model.TrainLosses, label='Train')
    ax.plot(model.TestLosses,  label='Test')
    ax.set_xlabel('Epoch')
    ax.set_ylabel('Losses')
    ax.legend(loc='best')
    return ax

def plotParams(ax, model):
    for i in range(model.nb_params):
        p = np.zeros(len(model.Params))
        for e in range(len(model.Params)):
            p[e] = model.Params[e][i].detach()
        ax.plot(p, label='param #%s' %i)
    ax.set_xlabel('Epoch')
    ax.set_ylabel('Params')
    ax.legend(loc='best')
    return ax

def plotMoments(ax, moms):
    for k in range(len(moms)):
        ax.plot(moms[k], label=r'$M^{(%s)}$' %(k+1))
    ax.set_xlabel('Epoch')
    ax.set_ylabel('Moments')
    ax.legend(loc='best')
    return ax


#TODO: add exact grid points if available
#TODO: make this able to use plotFunc instead
def plotDiff(ax, dataname, model, grid, xTr, fTr, xTe, fTe, interp='none'):
    X, F = challenges.load_challenge_exact(dataname)
    Feval = eval_model(grid, model, epoch=-1)
    #if model.type == 'mlp':
    #    Feval = [np.array(f) for f in Feval]
    if grid.dim == 1:
        # Reshaping shenaningans
        if model.type=='qnn':
            X, F = [x[0] for x in X], [f[0] for f in F] #FIXME is a patch, works for 1d only
        if model.type=='mlp':X, F, Feval = flatten(X,[]), flatten(F,[]), flatten(Feval,[])
        
        ax.plot(X,np.array(F)-np.array(Feval), label='LEGEND HERE')
        ax.set_xlabel(r'$x$')
        ax.set_ylabel(r'$f(x)-\hat{f}(x)$')
        ax.legend(loc='best')
    elif grid.dim == 2:
        print('F grid flipped?')
        FF = F# - Feval
        FFmin, FFmax = np.min(FF), np.max(FF)
        xtr, ytr = [], []
        xte, yte = [], []
        for xy in xTr:
            xtr.append(xy[0])
            ytr.append(xy[1])
        for xy in xTe:
            xte.append(xy[0])
            yte.append(xy[1])
        
        fig = plt.figure()
        im = ax.imshow(FF, origin='lower', interpolation=interp, extent=grid.extent, cmap='magma')
        
        ax.scatter(xtr, ytr, marker='P', label='Train')
        ax.scatter(xte, yte, marker='X', label='Test')
        ax.legend(loc='best')
        #ax.scatter(xTe[:][0], xTe[:][1], color='w')
        boundaries = np.linspace(FFmin, FFmax, 100) #cbar
        ticks = np.linspace(FFmin, FFmax, 5)
        cbar = fig.colorbar(im, ax=ax, boundaries=boundaries, ticks=ticks)
    return ax
   
#TODO: add exact grid points if available
#TODO: make this able to use plotFunc instead
def plotDiffOld(ax, dataname, model, grid, interp='gaussian'):
    X, F = challenges.load_challenge_exact(dataname)
    Feval = eval_model(grid, model, epoch=-1)
    #if model.type == 'mlp':
    #    Feval = [np.array(f) for f in Feval]
    if grid.dim == 1:
        if model.type=='qnn':
            X, F = [x[0] for x in X], [f[0] for f in F] #FIXME is a patch, works for 1d only
        if model.type=='mlp':X, F, Feval = flatten(X,[]), flatten(F,[]), flatten(Feval,[])
        plt.plot(X,np.array(F)-np.array(Feval))
        ax.set_xlabel(r'$x$')
        ax.set_ylabel(r'$f(x)-\hat{f}(x)$')
        ax.legend(loc='best')
    elif grid.dim == 2:
        #boundaries = np.linspace(grid.Fmin, grid.Fmax, 100) #cbar
        #ticks = np.linspace(grid.Fmin, grid.Fmax, 5)
        print('F grid flipped?')
        FF = F - Feval
        fig = plt.figure()
        im = ax.imshow(FF, origin='lower', interpolation=interp, extent=grid.extent, cmap='magma')
        #cbar = plt.colorbar(im, ax=ax, boundaries=boundaries, ticks=ticks)
        cbar = fig.colorbar(im, ax=ax)
    return ax
   
def parameters_to_np_array(model, idx):
    if model.type == 'mlp':
        # model.Params[idx] is an OrderedDict with keys=activation and vals=params
        # first, extract the vals, transform each val to np array then to list
        # this yields a list of lists, which can be unpacked
        res = [v.detach().numpy().tolist() for k, v in model.Params[idx].items()]
        #return torch.nn.utils.parameters_to_vector(model.Params[idx]).detach().numpy()
        return np.array(flatten(res, []))
    elif model.type == 'qnn':
        return np.array(model.Params[idx].detach().numpy() if idx > 0 else 0*model.Params[0].detach().numpy())


def calc_moments(model, K, idx=-1):
    P_idx = parameters_to_np_array(model, idx)
    epochs = len(model.Params)
    res = []
    for k in range(1,K):
        mom = []
        for epoch in range(epochs):
            P_e = parameters_to_np_array(model, epoch)
            #mom.append(np.sum(model.Params[e].detach().numpy()-Pidx)**k)
            #mom.append(np.sum(P_e - P_idx)**k)
            mom.append(np.sum(P_e)**k) #the most meaningful?
        res.append(mom)
    return res

def unzip(dataLoader):
    dataIn, dataOut = [], []
    for xBatch, fBatch in dataLoader:
        for x, f in zip(xBatch, fBatch):
            #dataIn.append(x) # works but raises a warning 
            #dataOut.append(f)# works but raises a warning
            dataIn.append(x.detach().numpy())
            dataOut.append(f.detach().numpy())
            #dataIn.append(float(x.detach().numpy())) #FIXME: does not work for data in more than 1 dim
            #dataOut.append(float(f.detach().numpy()))
    return np.array(dataIn, dtype='object'), np.array(dataOut, dtype='object')

def gen_grid(shape):
    dim = len(shape)
    # TODO: switch to meshgrid for fast evals?
    
    #NOTE: float32 needed for dtype compatibility with params
    X = list(np.linspace(-1, 1, shape[0], dtype='float32'))
    Y = list(np.linspace(-1, 1, shape[1], dtype='float32')) if dim>1 else None
    Z = list(np.linspace(-1, 1, shape[2], dtype='float32')) if dim>2 else None

    if dim == 1: return [[x] for x in X]
    elif dim == 2:
        return [[x,y] for x,y in product(X,Y)]
    elif dim == 3:
        return [[x,y,z] for x,y,z in product(X,Y,Z)]
    else:
        raise ValueError("Grid for %sd not implemented")

#def eval_model(model, X, nb_points, epoch=-1):
def eval_model(grid, model, epoch=-1):
    if isinstance(model, Model):
        seq = model.seq
        #params = parameters_to_np_array(model, epoch)
        params = model.Params[epoch].detach().numpy()
        # with analytical expr
        # TODO: modify the analytical module to handle n-dim data
        #if dim == 1:
        #    X = np.array(grid.X)
        #    angles = calc_angles(params, X)
        #    expr = calc_expr(seq)
        #    F = expr(*angles)
        
        # without analytical exprs
        F = []
        for x in grid.X:
            F.append(model.forward(torch.tensor([x], requires_grad=False)).detach().numpy())
        F = np.reshape(np.array(F), grid.nb_pts).T
        return F

    if isinstance(model, Mlp):
        model.eval()
        F = []
        for x in grid.X:
            pred = model.forward(torch.tensor(x, requires_grad=False))
            F.append(pred.detach().numpy())
        if model.input_size == 2:
            F = np.reshape(np.array(F), grid.nb_pts).T
        return F

def plotGif(model, grid):
    print('starting gif')
    print('Gif plot broken because eval_model() uses forward(), hence params cannot be modified')
    #TODO: gif from pdfs (jpgs are quickly blurry)
    #TODO: add target function on the side
    #TODO: pass epoch to eval_model
    #TODO: axes tuning
    #TODO: add train and test
    #TODO: interp 1d and 2d?
    imgs = []
    epochs = len(model.Params)
    print(epochs)

    # Eval the model over all epochs
    for epoch in tqdm(range(epochs), desc='Gifs, model eval'):
        imgs.append(eval_model(grid, model, epoch)) 
    valMin, valMax = np.min(imgs), np.max(imgs)

    # Plot and save
    for epoch in tqdm(range(epochs), desc='Gifs, .jpg plots'):
        fig = plt.figure()
        ax = fig.add_subplot(111)
        if grid.dim == 1:
            ax.plot(imgs[epoch])
        if grid.dim == 2:
            im = plt.imshow(imgs[epoch], origin='lower', interpolation=None)
            plt.clim(valMin, valMax)
            cbar = fig.colorbar(im)
        plt.text(0.1, 0.9, '(%s/%s)'%(epoch,epochs), transform=plt.gca().transAxes)
        plt.title('nice title')
        plt.savefig(str(epoch+1).zfill(6) + '.jpg') #pad nbr to some length, avoids ImageMagicK messing up the ordering
        plt.close()

    duration = 300 # milliseconds? apparently no
    delay = str(duration / (epochs-1))

    s = tm()
    os.system('convert -delay ' + delay + ' -loop 0 *jpg animated.gif')
    print(tm()-s)

def maketicks(vmin, vmax, n=5):
    return list(np.linspace(vmin, vmax, n))


