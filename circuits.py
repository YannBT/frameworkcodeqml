import pennylane as qml
from torch import dot, FloatTensor
from torch import tensor, Tensor
import convenience_functions as convf
import convenience_qnn as convq

dev = qml.device('default.qubit', wires=10) # CAVEAT: wires must be equal to or larger than the max number of planned qubits
#dev2 = qml.device('default.qubit', wires=2)
#dev4 = qml.device('default.qubit', wires=4)

# Convenience function for easier calls within the code
def get_circuit(name):
    if name == 'RUC': return RUC
    elif name == 'TTN2': return TTN2

@qml.qnode(dev, interface='torch')
def circuit4(angles):
    qml.RY(angles[0], wires=0)
    return qml.expval(qml.PauliZ(0))

@qml.qnode(dev, interface='torch')
def RUC(angles, model): #TODO: add Rot + all mono-qubit gates by order of how frequent
    # Checks 
    #TODO: takes too much runtime? <- no, each assert takes ~1% of the circuit's runtime
    assert(len(angles) == len(model.seq))
    assert(all([gate in convq.oneQubitGates for gate in model.seq]))

    # Operations
    for i in range(len(angles)):
        if model.seq[i] == 'RX':
            qml.RX(angles[i], wires=0)
        elif model.seq[i] == 'RY':
            qml.RY(angles[i], wires=0)
        elif model.seq[i] == 'RZ':
            qml.RZ(angles[i], wires=0)
        else:
            raise KeyError('Gate %s not implemented in circuits.RUC' %model.list_seq[i])
    return qml.expval(qml.PauliZ(0))

# TTN performing only two-qubits rotations
@qml.qnode(dev, interface='torch')
def TTN2(angles, model):#seq, n_wires, s=2): #s: downscaling factor
    '''
    This functions implements tree tensor networks (TTN) on quantum circuits.
    The sequence of rotations (model.seq) must contain two-qubits gates only.
    '''
    # Fetch the needed attributes
    seq = model.seq
    n_wires = model.nb_qubits
    s = model.downscaling

    # Convenience defs
    tracked = list(range(n_wires)) #tracked wires
    n_layers = model.depth #len(seq) # always true?
    n_rots = len(convf.flatten(seq,[]))

    # Checks
    print(seq)
    print('\n')
    print(n_wires)
    print(s)
    print(n_layers)
    assert(n_wires == s**n_layers)
    assert(n_rots  == (s**n_layers - 1)/(s - 1))

    # Operations
    for l in range(n_layers):
        i = 0 #number of layers gone through
        for ctrl, tgt in zip(tracked[::s], tracked[1::s]):
            gate = seq[l]
            rot = convq.getRot(gate)
            rot(angles[0], wires=[ctrl, tgt])
            #qml.RX(angles[0], wires=0)
            #qml.CRX(angles[0], wires=[ctrl,tgt])
            #[ctrl,tgt])
            #qml.CNOT(wires=[ctrl, tgt]) #TODO: ifs here
            i += 1
        tracked = tracked[::s]
    return qml.expval(qml.PauliZ(0)) #NOTE personal conv: ALWAYS manage to measure qubit zero

@qml.qnode(dev, interface='torch')
def TTN(angles, seq, n_wires, s=2, Hadamard=False): #s: downscaling factor
    '''
    Apply rotations according to a tree tensor network (TTN) architecture, with downscaling factor s.
    '''

    # If yes, rotate all qubits first. This allows using only controlled rotations. See the doc for more.
    if Hadamard:
        for qbit in range(n_wires):
            qml.Hadamard(qbit)

    #TODO: write description
    #TODO: define the appropriate gates
    # Convenience defs
    tracked = list(range(n_wires)) #tracked wires
    layers = len(seq) #true iff seq is a list of lists
    n_rots = sum([len(l) for l in seq])

    # Checks
    assert(n_wires == s**layers)
    assert(n_rots  == (s**layers - 1)/(s - 1))
    
    #TODO: gain runtime by externalising these?
    tracked = list(range(n_wires)) #tracked wires
    
    for l in layers:
        lists = [tracked[k::s] for k in range(s)]
        for pack in zip(lists):
            print(pack)
            #qml.GATE(*pack)
        tracked = tracked[::s]
    return qml.expval(qml.PauliZ(0)) #NOTE personal conv: ALWAYS manage to measure qubit zero







#old circuits + STILL USED conv functions
@qml.qnode(dev, interface='torch')
def RXRY(angles):
    for i in range(len(angles)):
        if i % 2 == 0:
            qml.RY(angles[i], wires=0)
        else:
            qml.RX(angles[i], wires=0)
    return qml.expval(qml.PauliZ(0))

@qml.qnode(dev, interface='torch')
def RXRZ(angles):
    for i in range(len(angles)):
        if i % 2 == 0:
            qml.RZ(angles[i], wires=0)
        else:
            qml.RX(angles[i], wires=0)
    return qml.expval(qml.PauliZ(0))


@qml.qnode(dev, interface='torch')
def RYRX(angles):
    for i in range(len(angles)):
        if i % 2 == 0:
            qml.RX(angles[i], wires=0)
        else:
            qml.RY(angles[i], wires=0)
    return qml.expval(qml.PauliZ(0))

@qml.qnode(dev, interface='torch')
def RYRZ(angles):
    for i in range(len(angles)):
        if i % 2 == 0:
            qml.RZ(angles[i], wires=0)
        else:
            qml.RY(angles[i], wires=0)
    return qml.expval(qml.PauliZ(0))



@qml.qnode(dev, interface='torch')
def RZRX(angles):
    for i in range(len(angles)):
        if i % 2 == 0:
            qml.RX(angles[i], wires=0)
        else:
            qml.RZ(angles[i], wires=0)
    return qml.expval(qml.PauliZ(0))



@qml.qnode(dev, interface='torch')
def RZRY(angles):
    for i in range(len(angles)):
        if i % 2 == 0:
            qml.RY(angles[i], wires=0)
        else:
            qml.RZ(angles[i], wires=0)
    return qml.expval(qml.PauliZ(0))


@qml.qnode(dev, interface='torch')
def Rot(angles):
    for i in range(0, len(angles), 3):
            qml.RZ(angles[i],   wires=0)
            qml.RX(angles[i+1], wires=0)
            qml.RZ(angles[i+2], wires=0)
    return qml.expval(qml.PauliZ(0))

# Get a string corresponding to the building blocks of the circuit
# Smart move: return the corresponding sequence too (very useful for analytical expr calc)
def name(circuit):
    if circuit == RXRY:   return 'RXRY', ['RX', 'RY']
    elif circuit == RXRZ: return 'RXRZ', ['RX', 'RZ']
    elif circuit == RYRX: return 'RYRX', ['RY', 'RX']
    elif circuit == RYRZ: return 'RYRZ', ['RY', 'RZ']
    elif circuit == RZRX: return 'RZRX', ['RZ', 'RX']
    elif circuit == RZRY: return 'RZRY', ['RZ', 'RY']
    elif circuit == Rot:  return 'Rot',  ['Rot']
    #elif circuit == RUC:  return 'RUC',  ['undefined']
    else:
        raise KeyError('Circuit not recognised: ', circuit)

def seq(seq, nb_gates):
    s = []
    for n in range(nb_gates):
        s += seq
    return s[:nb_gates]

    



