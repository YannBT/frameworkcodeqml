#def zeroParamGates():
#    return {'I': 0, 'H': 0, 'X': 0, 'Y': 'Z', 'S': 0, 'T': 0, 'SX': 0, 'CNOT': 0} #TODO: add all the avail gates
#def oneParamGates():
#    return {'RX': 1, 'RY': 1, 'RZ': 1, 'CRX': 1, 'CRY': 1, 'CRZ':1} #TODO: same as above

zeroParamGates = {'I': 0, 'H': 0, 'X': 0, 'Y': 0, 'Z': 0, 'S': 0, 'T': 0, 'SX': 0, 'CNOT': 0} #TODO: add all the avail gates
oneParamGates  =  {'RX': 1, 'RY': 1, 'RZ': 1, 'CRX': 1, 'CRY': 1, 'CRZ':1} #TODO: same as above

oneQubitGates = ['I', 'H', 'X', 'Y', 'Z', 'RX', 'RY', 'RZ', 'Rot', 'S', 'T', 'SX'] #TODO:same as above
twoQubitGates = ['CNOT', 'CRX', 'CRY', 'CRZ']

nbrParamGates = {**zeroParamGates, **oneParamGates}
ParametricGates = {**oneParamGates}

import pennylane as qml 
PL1q = {'I': qml.Identity, 'H': qml.Hadamard, 'X': qml.PauliX, 'Y': qml.PauliY, 'Z': qml.PauliZ, 'S': qml.S, 'T': qml.T, 'SX': qml.SX}
PL2q = {'CRX': qml.CRX, 'CRY': qml.CRY, 'CRZ': qml.CRZ, 'CNOT': qml.CNOT, 'Rot': qml.Rot}

PL0p = {'I': qml.Identity, 'H': qml.Hadamard, 'X': qml.PauliX, 'Y': qml.PauliY, 'Z': qml.PauliZ, 'S': qml.S, 'T': qml.T, 'SX': qml.SX, 'CNOT': qml.CNOT}
PL1p = {'RX': qml.RX, 'RY': qml.RY, 'RZ': qml.RZ, 'CRX': qml.CRX, 'CRY': qml.CRY, 'CRZ': qml.CRZ}
PL2p = {}
PL3p = {'Rot': qml.Rot}


dicToPLq = {**PL1q, **PL2q}
dicToPLp = {**PL0p, **PL1p, **PL2p, **PL3p}

# Check that the keys are linked to a nbr of qubits and a nbr of parameters
for name, gate in dicToPLq.items():
    q1 = name in PL1q.keys()
    q2 = name in PL2q.keys()
    q3 = False#name in PL3q.keys()

    p0 = name in PL0p.keys()
    p1 = name in PL1p.keys()
    p2 = name in PL2p.keys()
    p3 = name in PL3p.keys()

    assert([q1,q2,q3].count(True) == 1)
    assert([p0,p1,p2,p3].count(True) == 1)

def getRot(rot):
    '''
    Return a PennyLane rotation given a string.
    '''
    return dicToPLq[rot]

# Flatten a list of lists. Used for flattening sequences,
# e.g. ['RX','RY'] -> unchanged, [['RX', 'RY'], 'CNOT'] -> ['RX','RY','CNOT']
# Currently used to determine the number of rotations of a circuit
def flatten(l,flat):
    for sub in l:
        if isinstance(sub, list):
            flatten(sub,flat)
        else:
            flat.append(sub)
    return flat

    


def isConsistent(dic):
    # Quantum networks need to have their rotations checked w.r.t the circuit
    circ  = dic['circuit']
    seq   = dic['seq']
    depth = dic['depth']
    nq    = dic['qubit']
    if circ == 'RUC':
        # Consistency rules for RUC:
        # - only one qubit
        # - only 1 qubit rotations
        # - at least one differentiable gate
        oneQubit    = (nq == 1)
        oneQubitRot = all([g in oneQubitGates for g in seq])
        isDiff      = any([g in ParametricGates for g in seq])
        if oneQubit and oneQubitRot and isDiff:
            return True
        else:
            return False
    elif circ == 'TTN2':
        # Consistency rules for TTN: 
        # - only 2 qubits rotations
        # - at least one differentiable gate
        # - n_wires == downscaling**n_layers
        # - n_rots  == (downscaling**n_layers - 1)/(downscaling - 1) <- not checked here
        onlyTwo = all([g in twoQubitGates for g in seq])
        isDiff   = any([g in ParametricGates for g in seq])
        nWires = (nq == 2**depth)
        if onlyTwo and isDiff and nWires:
            return True
        else: 
            if not onlyTwo:
                print('This circuit only accepts two-qubit gates')
            if not isDiff:
                print('The circuit must be differentiable')
            if not nWires:
                print('The depth of the circuit and the number of qubits are not consistent')
            return False
    elif circ == 'MERA':
        #NOTE: this is not TTN but MERA... the former do not include single-qubit gates

        # Consistency rules for TTN2: 
        # - only 1 or 2 qubit rotations
        # - at least one gate acting one more than 1 qubit
        # - at least one differentiable gate
        # - n_wires == downscaling**n_layers
        # - n_rots  == (downscaling**n_layers - 1)/(downscaling - 1) <- not checked here
        oneTwoQubit = all([g in oneQubitGates or g in twoQubitGates for g in seq])
        notOnlyOne = any([g not in oneQubitGates for g in seq])
        notOnlyTwo = any([g not in twoQubitGates for g in seq])
        isDiff   = any([g in ParametricGates for g in seq])
        nWires = (nq == 2**depth)
        if oneTwoQubit and notOnlyOne and isDiff and nWires:
            return True
        else: 
            if not oneTwoQubit:
                print('This circuit only accepts one- and two-qubits gates')
            if not notOnlyOne:
                print('This circuit does not work with one-qubit gates only')
            if not notOnlyTwo:
                print('This circuit does not work with two-qubit gates only')
            if not isDiff:
                print('The circuit must be differentiable')
            if not nWires:
                print('The depth of the circuit and the number of qubits are not consistent')
            return False
    else:
        raise KeyError('Circuit %s not recognised'%circ)









